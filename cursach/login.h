#ifndef LOGIN_H
#define LOGIN_H
#include <QDialog>
#include <QTcpSocket>
#include <recuest.h>
namespace Ui {
class login;
}

class login : public QDialog
{
        Q_OBJECT

public:
    explicit login(QTcpSocket *client,QWidget *parent = 0);
    ~login();
    bool status;
    bool onClose;
    bool get_status();
    int ID;
    QByteArray Data;
    QTcpSocket * client;

private slots:

    void on_buttonBox_rejected();

    void on_pushButton_2_clicked();

    void onConnected();

    void onReadyRead();

    void onBytesWritten(qint64 n);

    void onDisconnected();

    void on_pushButton_3_clicked();

    void checkLogin_true(recuest * rec);

    void on_pushButton_clicked();

private:

    Ui::login *ui;

};

#endif // LOGIN_H
