#ifndef PAGE_H
#define PAGE_H

#include <QDialog>

#include <information.h>
#include <user_albom.h>

namespace Ui {
class page;

}

class page : public QDialog
{
    Q_OBJECT
    RData info;
    std::vector<albom>alboms;
public:
    explicit page( RData info, std::vector<albom>alboms,int id, QWidget *parent = 0);
    ~page();
    int ID;
    void set();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

private:
    Ui::page *ui;

};

#endif // PAGE_H
