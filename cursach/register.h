#ifndef REGISTER_H
#define REGISTER_H

#include <QTcpSocket>
#include <QDialog>

struct RegisterData {
    QString login;
    QString password;
    QString name;
    QString surname;
    QString city;
    QString hobby;
    QString religion;
    QString birth;
    QByteArray pic;
    QByteArray MainPic;
};

namespace Ui {
class Register;
}

class Register : public QDialog
{
    Q_OBJECT

public:
    explicit Register(QTcpSocket * client, QWidget *parent = 0);
    ~Register();
    RegisterData registerData();
    QByteArray getPic;
    QByteArray getMainPic;
    void setDefaultPic();
    bool added;
    QByteArray Data;
    std::vector <QString> users;
    QTcpSocket * client;
    QString secretQuestion;
    QString secretAnswer;

    void preparing();

private slots:
    void on_buttonBox_accepted();

    void on_pushButton_clicked();

    void validate();

    bool validatePassword();

    void on_lineEdit_textChanged(const QString &arg1);

    void on_lineEdit_2_textChanged(const QString &arg1);

    void on_lineEdit_3_textChanged(const QString &arg1);

    void on_lineEdit_4_textChanged(const QString &arg1);

    void on_lineEdit_5_textChanged(const QString &arg1);

    void on_lineEdit_6_textChanged(const QString &arg1);

    void onReadyRead();

    bool validateLogin(QString x);

    void on_lineEdit_8_textChanged(const QString &arg1);

    void on_lineEdit_9_textChanged(const QString &arg1);

private:
    Ui::Register *ui;
};

#endif // REGISTER_H
