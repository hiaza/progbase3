#include "login.h"
#include "ui_login.h"
#include <iostream>
#include <QMessageBox>
#include "register.h"
#include "user.h"
#include "recoveringpass.h"

using namespace std;
login::login(QTcpSocket *client,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::login)
{
    ui->setupUi(this);
    status = false;
    onClose = false;
    connect(client, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(client, SIGNAL(bytesWritten(qint64)), this, SLOT(onBytesWritten(qint64)));
    connect(client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(client, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    this->client = client;
}

void login::onConnected() {
    cout << "connected" << endl;
}


void login::onReadyRead() {

    cout << "ready to read" << endl;

    Data.append(client->readAll());

    if(Data.right(6)=="@#%$#@"){

        QString temp = QString::fromStdString(Data.toStdString());

        temp.chop(6);

        recuest * rec;

        rec = rec->deserializeRecuest(temp);

        if (rec->type == RT_CHEAK_LOGIN){

            checkLogin_true(rec);

        }else if (rec->type == RT_ADD_USER){

            QMessageBox::information(this,tr("info"),tr("Успішно зареєстровано"));

        }

        else if(rec->type == RT_NONE){

            QMessageBox::warning(this,tr("Помилка"),rec->message);

        }
        delete rec;

        cout << "Received:" << endl << Data.toStdString() << endl;

        Data.clear();
    }

    //client->disconnectFromHost();

}

void login::checkLogin_true(recuest * rec){
    status = true;
    onClose = true;
    ID = rec->sender_id;

    disconnect(client, SIGNAL(connected()), this, SLOT(onConnected()));

    disconnect(client, SIGNAL(bytesWritten(qint64)), this, SLOT(onBytesWritten(qint64)));

    disconnect(client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

    disconnect(client, SIGNAL(disconnected()), this, SLOT(onDisconnected()));

    this->accept();
}


void login::onBytesWritten(qint64 n) {
    cout << "bytes  written " << endl;
}

void login::onDisconnected() {
    cout << "disconnected" << endl;
    QTcpSocket * clientSocket = static_cast<QTcpSocket*>(sender());
    //delete clientSocket;
}




login::~login()
{
    delete ui;
}

bool login::get_status(){
    return status;
}

void login::on_buttonBox_rejected()
{
    onClose = true;
}

void login::on_pushButton_2_clicked()
{
    disconnect(client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    Register window(client);
    window.preparing();
    int res = window.exec();
    connect(client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    if (res == QDialog::Accepted) {
            RegisterData dat = window.registerData();
            vector <RData> y;
            RData info;
            info.name = dat.name;
            info.surname = dat.surname;
            info.birth = dat.birth;
            info.city = dat.city;
            info.hobby = dat.hobby;
            info.id = -1;
            info.religion = dat.religion;
            info.pic = dat.pic;
            info.MainPic = dat.MainPic;
            LoginData data;
            data.login = dat.login;
            data.password = dat.password;
            ID = 0;
            y.push_back(info);
            RData quest;
            quest.name = window.secretQuestion;
            quest.surname = window.secretAnswer;
            y.push_back(quest);
            recuest * x = new recuest(RT_ADD_USER, ID,-1,vector<albom>(),vector<QPixmap>(), y , data, "" ,QPixmap());
            QString requestStr = x->serializeRecuest();
            requestStr = requestStr + "@#%$#@";
            client->waitForConnected(500);
                cout << "Sending: " << endl << requestStr.toStdString() << endl;
                client->write(requestStr.toUtf8());
                client->flush();
                if(!client->waitForReadyRead(2000)) QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));
    }
}


void login::on_pushButton_3_clicked()
{

    LoginData data;
    data.login = ui->lineEdit->text();
    data.password = ui->lineEdit_2->text();
    ID = 0;

    recuest * x = new recuest(RT_CHEAK_LOGIN, ID,-1,vector<albom>(),vector<QPixmap>(),vector<RData>(), data, "" ,QPixmap());
    QString requestStr = x->serializeRecuest();
    requestStr = requestStr + "@#%$#@";
    client->waitForConnected(500);
    cout << "Sending: " << endl << requestStr.toStdString() << endl;
    client->write(requestStr.toUtf8());
    client->flush();
    if(!client->waitForReadyRead(1000)) QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

}

void login::on_pushButton_clicked()
{
    recoveringPass win;
    win.exec();
}
