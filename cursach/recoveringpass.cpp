#include "recoveringpass.h"
#include "ui_recoveringpass.h"
#include "recuest.h"
#include "iostream"
#include <QMessageBox>

using namespace std;

recoveringPass::recoveringPass(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::recoveringPass)
{
    ui->setupUi(this);
    QTcpSocket * client = new QTcpSocket();
    client->connectToHost("127.0.0.1",3000);
    this->client = client;

    connect(this->client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

    ui->lineEdit->setEnabled(true);
    ui->pushButton->setEnabled(true);
    ui->pushButton_2->hide();
    ui->pushButton_3->hide();
    ui->answer1->hide();
    ui->quest1->hide();
    ui->lineEdit_2->hide();
    ui->label_2->hide();
    ui->label_5->hide();
}

recoveringPass::~recoveringPass()
{
    client->disconnectFromHost();
    delete client;
    delete ui;
}

void recoveringPass::onReadyRead() {

    cout << "-----------------------------------------ready to read---------------------------------------------------" << endl;

    Data.append(client->readAll());

    if(Data.right(6)=="@#%$#@"){

        QString temp = QString::fromStdString(Data.toStdString());

        temp.chop(6);

        recuest * rec;

        rec = rec->deserializeRecuest(temp);

        if (rec->type == RT_RECOVER_PASS){

            onCorrect_login(rec);

        }else if (rec->type == RT_RECOVER_PASS2){

            onCorrectAnswer(rec);

        }else if (rec->type == RT_RECOVER_PASS3){

            Exit_Successs(rec);

        }
        delete rec;

        cout << "Received:" << endl << Data.toStdString() << endl;

        Data.clear();
    }
}

void recoveringPass::onCorrectAnswer(recuest * rec){

    if(rec->reciever_id == -1){
        QMessageBox::information(this,"Error","Неправильна відповідь");
        return;
    }
    ui->answer1->setEnabled(false);
    ui->answer1->hide();
    ui->label_2->hide();
    ui->label_2->setEnabled(false);
    ui->quest1->hide();
    ui->quest1->setEnabled(false);
    ui->pushButton_2->hide();
    ui->pushButton_2->setEnabled(false);


    ui->label_5->show();
    ui->label_5 ->setEnabled(true);
    ui->lineEdit_2->show();
    ui->lineEdit_2->setEnabled(true);
    ui->pushButton_3->show();
    ui->pushButton_3->setEnabled(true);

}

void recoveringPass::Exit_Successs(recuest * rec){

    if(rec->reciever_id == -1){
        QMessageBox::information(this,"Error","Помилка");
    }else{
        QMessageBox::information(this,"INFO","Успішно змінений");
    }
    this->close();
}

void recoveringPass::onCorrect_login(recuest * rec){

    if(rec->sender_id == -1){
        QMessageBox::information(this,"Error","Немає такого логіну");
        return;
    }
    id = rec->sender_id;
    QString quest = rec->logData.login;
    ui->lineEdit->setEnabled(false);
    ui->pushButton->setEnabled(false);
    ui->lineEdit->hide();
    ui->pushButton->hide();
    ui->answer1->setEnabled(true);
    ui->answer1->show();
    ui->label_3->hide();
    ui->label_2->show();
    ui->label_2->setEnabled(true);
    ui->quest1->show();
    ui->quest1->setText(quest);
    ui->quest1->setEnabled(true);
    ui->pushButton_2->show();
    ui->pushButton_2->setEnabled(true);

}

void recoveringPass::on_pushButton_clicked()
{
    LoginData temp;

    temp.login = ui->lineEdit->text();

    recuest * x = new recuest(RT_RECOVER_PASS, -1, -1,vector<albom>(),
                              vector<QPixmap>(),vector<RData>(), temp, "" ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    client->waitForConnected(500);

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    delete x;
}

void recoveringPass::on_pushButton_2_clicked()

{
    recuest * x = new recuest(RT_RECOVER_PASS2, id, -1,vector<albom>(),
                             vector<QPixmap>(),vector<RData>(),LoginData() , ui->answer1->text() ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    client->waitForConnected(500);

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    delete x;
}

void recoveringPass::on_pushButton_3_clicked()
{
    recuest * x = new recuest(RT_RECOVER_PASS3, id, -1,vector<albom>(),
                             vector<QPixmap>(),vector<RData>(),LoginData() , ui->lineEdit_2->text() ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    client->waitForConnected(500);

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    delete x;
}

void recoveringPass::on_pushButton_4_clicked()
{
    this->close();
}
