#-------------------------------------------------
#
# Project created by QtCreator 2018-05-11T03:54:16
#
#-------------------------------------------------

QT       += core gui network
QT += sql

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cursach
TEMPLATE = app


SOURCES += main.cpp\
        mainpage.cpp \
    login.cpp \
    register.cpp \
    clipscene.cpp \
    photo.cpp \
    clickablelabel.cpp \
    albomv.cpp \
    albomcr.cpp \
    page.cpp \
    information.cpp \
    user_albom.cpp \
    recoveringpass.cpp

HEADERS  += mainpage.h \
    login.h \
    register.h \
    clipscene.h \
    photo.h \
    clickablelabel.h \
    albomv.h \
    albomcr.h \
    page.h \
    information.h \
    user_albom.h \
    recoveringpass.h

FORMS    += mainpage.ui \
    login.ui \
    register.ui \
    photo.ui \
    albomv.ui \
    albomcr.ui \
    page.ui \
    information.ui \
    user_albom.ui \
    recoveringpass.ui

RESOURCES += \
    imgs.qrc

INCLUDEPATH += ../curLib
LIBS += -L ../build-curLib-Desktop-Debug -lcurLib
