#include "page.h"
#include "ui_page.h"
#include <QDebug>

page::page(RData info, std::vector<albom>alboms,int id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::page)
{
    ui->setupUi(this);
    ID = id;
    this->info = info;
    this->alboms = alboms;
}

void page:: set(){

        QString str = info.name + " " + info.surname;

        QPixmap outPixmap = QPixmap();

        outPixmap.loadFromData(info.MainPic);

        ui->label->setPixmap(outPixmap);

        ui->label_2 ->setText(str);

        ui->label_3->setText("City: " + info.city);

}


page::~page()
{
    delete ui;
}

void page::on_pushButton_2_clicked()
{
    Information p(ID);

    QString pageName = "ID"+ QString::number(ID) + "   " + info.name + " " + info.surname;
    p.setWindowTitle(pageName);

    p.set(info);
    p.exec();
}

void page::on_pushButton_3_clicked()
{
    close();
}

void page::on_pushButton_clicked()
{
    user_albom temp(alboms,ID);
    QString pageName = "ID"+ QString::number(ID) + "   " + info.name + " " + info.surname;
    temp.setWindowTitle(pageName);
    temp.setAlboms();
    temp.exec();
}
