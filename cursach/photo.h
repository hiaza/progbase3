#ifndef PHOTO_H
#define PHOTO_H

#include <QDialog>
class QGridLayout;
class QPushButton;
class QGraphicsView;
class QLabel;
class ClipScene;

namespace Ui {
class photo;
}

class photo : public QDialog
{
    Q_OBJECT

public:
    explicit photo(QString way, QWidget *parent = 0);
    ~photo();
    QPixmap poto;
    QPixmap mainPoto;
    bool saved;
private slots:
   // void on_pushButton_2_clicked();

   // void on_pushButton_clicked();

    void clickedOnSave();

    void onClippedImage(const QPixmap& pixmap);

private:
    Ui::photo *ui;
    QGridLayout* m_gridLayout;
    QPushButton* save_button;
    QGraphicsView* m_graphicsView;
    QLabel* m_clippedLabel;
    ClipScene* m_clipScene;
};

#endif // PHOTO_H
