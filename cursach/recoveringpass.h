#ifndef RECOVERINGPASS_H
#define RECOVERINGPASS_H

#include <QTcpSocket>
#include <QDialog>
#include <recuest.h>
namespace Ui {
class recoveringPass;
}

class recoveringPass : public QDialog
{
    Q_OBJECT

public:
    explicit recoveringPass(QWidget *parent = 0);
    ~recoveringPass();
    QByteArray Data;
    QTcpSocket * client;
private slots:
    void onReadyRead();
    void on_pushButton_clicked();
    void onCorrect_login(recuest * rec);
    void on_pushButton_2_clicked();
    void onCorrectAnswer(recuest * rec);
    void on_pushButton_3_clicked();
    void Exit_Successs(recuest * rec);

    void on_pushButton_4_clicked();

private:
    int id;
    Ui::recoveringPass *ui;
};

#endif // RECOVERINGPASS_H
