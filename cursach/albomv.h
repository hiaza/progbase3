#ifndef ALBOMV_H
#define ALBOMV_H

#include <QDialog>
#include <QPixmap>
#include <QLabel>
#include <QTcpSocket>
#include <recuest.h>
namespace Ui {
class AlbomV;
}

class AlbomV : public QDialog
{
    Q_OBJECT

public:
    explicit AlbomV(bool hide, QWidget *parent = 0);
    void preparing(std::vector<QPixmap> pict);
    ~AlbomV();
    std::vector<QPixmap> pictures;
    int albomId;
    QString name;
    QByteArray Data;
private slots:
    void on_buttonBox_accepted();

    void on_next_clicked();

    void on_prev_clicked();

    void cleanL();

    void on_add_clicked();

    void onReadyRead();

    void addPic(recuest * rec);

private:
    std::vector<QLabel*> photos;
    Ui::AlbomV *ui;
    int i;
    int incr = 9;
    QTcpSocket * client;
};

#endif // ALBOMV_H
