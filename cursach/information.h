#ifndef INFORMATION_H
#define INFORMATION_H

#include <QDialog>
#include <recuest.h>

namespace Ui {
class Information;
}

class Information : public QDialog
{
    Q_OBJECT

public:
    explicit Information(int id, QWidget *parent = 0);
    ~Information();
    void set(RData temp);

private:
    Ui::Information *ui;
    int ID;
};

#endif // INFORMATION_H
