#ifndef EWIDGET_H
#define EWIDGET_H

#include <QWidget>
class QGridLayout;
class QPushButton;
class QGraphicsView;
class QLabel;
class ClipScene;


class EWidget : public QWidget
{
    using BaseClass = QWidget;

    Q_OBJECT

public:
    EWidget(QPixmap& x, QWidget* parent = nullptr);
    QPixmap cropp;

private slots:
    //void onAddFile();
    //void onSaveButton(QPixmap &pixmap);// Слот добавления изображения в приложение
    void onClippedImage(const QPixmap& pixmap); // Слот принимающий обрезанную область приложения

private:
    QGridLayout* m_gridLayout;
    //QPushButton* m_pushButton;
    QPushButton* save_button;
    QGraphicsView* m_graphicsView;
    QLabel* m_clippedLabel;         // Лейбл, в который будет помещаться обрезанное изображение
    ClipScene* m_clipScene;
   // Графическая сцена в которой реализован функционал по обрезке изображения
};

#endif // EWIDGET_H
