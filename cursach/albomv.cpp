#include "albomv.h"
#include "ui_albomv.h"
#include <QBuffer>
#include <QDebug>
#include <QFileDialog>
#include <iostream>
#include <QMessageBox>
using namespace std;

AlbomV::AlbomV(bool hide,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AlbomV)
{
    ui->setupUi(this);

    QTcpSocket * client = new QTcpSocket();
    client->connectToHost("127.0.0.1",3000);
    this->client = client;

    photos.push_back(ui->label);
    photos.push_back(ui->label_2);
    photos.push_back(ui->label_3);
    photos.push_back(ui->label_4);
    photos.push_back(ui->label_5);
    photos.push_back(ui->label_6);
    photos.push_back(ui->label_7);
    photos.push_back(ui->label_8);
    photos.push_back(ui->label_9);
    if(hide){
        ui->add->hide();
    }
    connect(this->client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

}

void AlbomV::onReadyRead() {

    cout << "-----------------------------------------ready to read---------------------------------------------------" << endl;

    Data.append(client->readAll());

    if(Data.right(6)=="@#%$#@"){

        QString temp = QString::fromStdString(Data.toStdString());

        temp.chop(6);

        recuest * rec;

        rec = rec->deserializeRecuest(temp);

        if (rec->type == RT_ADD_PIC_TO_ALBOM){

            addPic(rec);

        }
        delete rec;

        cout << "Received:" << endl << Data.toStdString() << endl;

        Data.clear();
    }

}

void AlbomV::addPic(recuest * rec){

    pictures.push_back(rec->p);

    int place = (pictures.size()-1)%9;

    if(i+1>pictures.size()){

        photos.at(place)->setPixmap(pictures.at(pictures.size()-1));

    }else{

        ui->next->setEnabled(true);

    }
}

void AlbomV::on_add_clicked()
{
    QString way =  QFileDialog::getOpenFileName(0, "Open Dialog", "", "*.jpg *.png");

    if(way.length()>1){

        QPixmap img (way);

        recuest * x = new recuest(RT_ADD_PIC_TO_ALBOM, -1, albomId,vector<albom>(),vector<QPixmap>(),vector<RData>(), LoginData(), "" , img);

        QString requestStr = x->serializeRecuest();

        requestStr = requestStr + "@#%$#@";

        cout << "Sending: " << endl << requestStr.toStdString() << endl;

        client->write(requestStr.toUtf8());

        client->flush();

        if(!client->waitForReadyRead(4000)) {

            QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

            close();

        }
        delete x;
    }
}


void AlbomV::preparing(std::vector<QPixmap> pict){
    i = 9;
    for(int j = 0; j < i && j<pict.size();j++){
        photos.at(j)->setPixmap(pict.at(j));
    }
    pictures = pict;
    ui->name->setText(name);
}

AlbomV::~AlbomV()
{
    client->disconnectFromHost();
    delete client;
    delete ui;
}

void AlbomV::on_buttonBox_accepted()
{
    close();
}

void AlbomV::cleanL(){
    for(int j = 0; j<9; j++){
        photos.at(j)->clear();
    }
}

void AlbomV::on_next_clicked()
{
    if(i<pictures.size()){
        cleanL();
        qDebug() << i;
        int border = i;
        i =  i +  incr;
        int counter = 0;

        for(border; border < i && border < pictures.size();border++){
            photos.at(counter)->setPixmap(pictures.at(border));
            counter++;
        }
        qDebug() << i;
        qDebug() << border;

        ui->prev->setEnabled(true);
        if(border == pictures.size()){
            ui->next->setEnabled(false);
        }
    }
}

void AlbomV::on_prev_clicked()
{
    if(i>9){

        cleanL();

        i = i - incr;
        int border = i - incr;
        qDebug() << i;
        qDebug() << i;
        int counter = 0;
        for(border; border < i; border++){
            photos.at(counter)->setPixmap(pictures.at(border));
            counter++;
        }
        ui->next->setEnabled(true);
        if(i==9){
            ui->prev->setEnabled(false);
        }

    }
}
