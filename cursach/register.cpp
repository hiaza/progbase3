#include "register.h"
#include "ui_register.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QBuffer>
#include <photo.h>
#include <recuest.h>
#include <iostream>


using namespace std;

Register::Register(QTcpSocket * client, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Register)

{
    ui->setupUi(this);
    this->client = client;

    ui->buttonBox->setEnabled(true);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

    connect(client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

void Register::preparing(){

    LoginData data;
    data.login = " ";
    data.password = " ";

    recuest * x = new recuest(RT_GET_USERS, -1 ,-1,vector<albom>(),vector<QPixmap>(),vector<RData>(), data, "" ,QPixmap());
    QString requestStr = x->serializeRecuest();
    requestStr = requestStr + "@#%$#@";
    client->waitForConnected(500);
    cout << "Sending: " << endl << requestStr.toStdString() << endl;
    client->write(requestStr.toUtf8());
    client->flush();
}

void Register::onReadyRead() {

    cout << "ready to read" << endl;

    Data.append(client->readAll());

    if(Data.right(6)=="@#%$#@"){

        QString temp = QString::fromStdString(Data.toStdString());

        temp.chop(6);

        recuest * rec;

        rec = rec->deserializeRecuest(temp);

        if (rec->type == RT_GET_USERS){
            for(int i = 0; i < rec->data.size(); i++){
                users.push_back(rec->data.at(i).name);
                qDebug() << rec->data.at(i).name;
            }
            disconnect(client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
        }

        delete rec;
        Data.clear();
    }
}

Register::~Register()
{
    delete ui;
}

void Register::on_buttonBox_accepted()
{

}

void Register::on_pushButton_clicked()
{
    QString way =  QFileDialog::getOpenFileName(0, "Open Dialog", "", "*.jpg *.png");
    if(way.length()<2){
        return;
    }
    photo x(way);
    x.exec();
    if(x.saved){
        QPixmap cropped = x.poto;
        QByteArray inByteArray;
        QBuffer inBuffer( &inByteArray );
        inBuffer.open( QIODevice::WriteOnly );
        cropped.save( &inBuffer, "JPG");
        getPic = inByteArray;
        QPixmap original = x.mainPoto;
        QByteArray inByteArray2;
        QBuffer inBuffer2( &inByteArray2 );
        inBuffer2.open( QIODevice::WriteOnly );
        original.save( &inBuffer2, "JPG");
        getMainPic = inByteArray2;
        added = true;
        ui->label_11->setPixmap(cropped);

    }
}

void Register::setDefaultPic(){
    QString way = ":/new/im/default.jpeg";
    QPixmap img (way);
    QByteArray inByteArray;
    QBuffer inBuffer( &inByteArray );
    inBuffer.open( QIODevice::WriteOnly);
    img.save( &inBuffer, "JPG");
    getPic = inByteArray;
    getMainPic = inByteArray;
}
RegisterData Register::registerData() {
    RegisterData data;
    data.login = ui->lineEdit_3->text();
    data.password = ui->lineEdit_4->text();
    data.name = ui->lineEdit->text();
    data.surname = ui->lineEdit_2->text();
    data.city = ui->lineEdit_6->text();
    data.hobby = ui->textEdit->toPlainText();
    data.religion = ui ->lineEdit_7->text();
    data.birth = ui->dateEdit->text();
    secretQuestion = ui->lineEdit_8->text();
    secretAnswer = ui->lineEdit_9->text();
    if(!added){
        setDefaultPic();
    }
    data.pic = getMainPic;
    data.MainPic = getPic;
    return data;
}


bool Register:: validateLogin(QString x){
    for (int i = 0 ; i < users.size(); i++){
        if(users.at(i) == x){
            qDebug() << "такий логін є";
            return false;
        }
    }
    return true;

}


void Register::validate(){
    bool invalid_password = validatePassword();
    bool invalidInput = ui -> lineEdit->text().isEmpty()
            || ui->lineEdit_2->text().isEmpty()
            || ui->lineEdit_3->text().isEmpty()
            || ui->lineEdit_6->text().isEmpty()
            || ui->lineEdit_8->text().isEmpty()
            || ui->lineEdit_9->text().isEmpty()
            || !validateLogin(ui->lineEdit_3->text())
            || invalid_password;
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!invalidInput);
}

bool Register::validatePassword(){
    bool invalidInput = ui -> lineEdit_4->text().isEmpty()
            || ui->lineEdit_5->text().isEmpty()
            || ui->lineEdit_4->text().compare(ui->lineEdit_5->text())!=0;
    return invalidInput;
}



void Register::on_lineEdit_textChanged(const QString &arg1)
{
    validate();
}

void Register::on_lineEdit_2_textChanged(const QString &arg1)
{
    validate();
}

void Register::on_lineEdit_3_textChanged(const QString &arg1)
{
    validate();
}

void Register::on_lineEdit_4_textChanged(const QString &arg1)
{
    validate();
}

void Register::on_lineEdit_5_textChanged(const QString &arg1)
{
    validate();
}

void Register::on_lineEdit_6_textChanged(const QString &arg1)
{
    validate();
}



void Register::on_lineEdit_8_textChanged(const QString &arg1)
{
    validate();
}


void Register::on_lineEdit_9_textChanged(const QString &arg1)
{
    validate();
}
