#ifndef NLOG_H
#define NLOG_H

#include <QDialog>

namespace Ui {
class Nlog;
}

class Nlog : public QDialog
{
    Q_OBJECT

public:
    explicit Nlog(QWidget *parent = 0);
    ~Nlog();

private:
    Ui::Nlog *ui;
};

#endif // NLOG_H
