#include "user_albom.h"
#include "ui_user_albom.h"
#include <QSqlQuery>
#include <QDebug>
#include <albomv.h>
#include <iostream>

using namespace std;

user_albom::user_albom(std::vector<albom> alboms, int id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::user_albom)
{
    ui->setupUi(this);

    QTcpSocket * client = new QTcpSocket();
    client->connectToHost("127.0.0.1",3000);
    this->clientSocket = client;
    this->alboms = alboms;
    ID = id;

    labels.push_back(ui->label);
    labels.push_back(ui->label_2);
    labels.push_back(ui->label_3);
    connect(ui->label,SIGNAL(clicked()),this,SLOT(on_test()));
    connect(ui->label_2,SIGNAL(clicked()),this,SLOT(on_test()));
    connect(ui->label_3,SIGNAL(clicked()),this,SLOT(on_test()));

    connect(this->clientSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));

}

void user_albom::onReadyRead() {

    cout << "-----------------------------------------ready to read---------------------------------------------------" << endl;

    Data.append(clientSocket->readAll());

    if(Data.right(6)=="@#%$#@"){

        QString temp = QString::fromStdString(Data.toStdString());

        temp.chop(6);

        recuest * rec;

        rec = rec->deserializeRecuest(temp);

        if (rec->type == RT_GET_PHOTOS_FROM_ALBOM){

            P_openChoosenPic(rec);

        }
        delete rec;

        cout << "Received:" << endl << Data.toStdString() << endl;

        Data.clear();
    }

}


void user_albom:: P_openChoosenPic(recuest * rec){

    albom te = alboms.at(rec->sender_id);

    AlbomV m(true);

    m.name = te.albom_name;

    m.preparing(rec->photos);

    m.albomId = te.albom_id;

    m.exec();
}


void user_albom:: on_test(){

     ClickableLabel * label = static_cast<ClickableLabel*>(sender());

     int p = ui->horizontalLayout->indexOf(label);

     if(i-3+p >= alboms.size()){
         return;
     }

     albom te = alboms.at(i-3+p);

     recuest * x = new recuest(RT_GET_PHOTOS_FROM_ALBOM, i-3+p, te.albom_id,vector<albom>(),
                               vector<QPixmap>(),vector<RData>(), LoginData(), "" ,QPixmap());

     QString requestStr = x->serializeRecuest();

     requestStr = requestStr + "@#%$#@";

     clientSocket->waitForConnected(500);

     cout << "Sending: " << endl << requestStr.toStdString() << endl;

     clientSocket->write(requestStr.toUtf8());

     clientSocket->flush();

     delete x;

}


user_albom::~user_albom()
{
    clientSocket->disconnectFromHost();
    delete clientSocket;
    delete ui;
}
void user_albom:: setAlboms(){

    i = 3;

    for(int j = 0; j < i && j<alboms.size();j++){
        QPixmap outPixmap = QPixmap();
        outPixmap.loadFromData(alboms.at(j).albom_pic);
        labels.at(j)->setPixmap(outPixmap);
    }
}

void user_albom:: cleanL(){
    for(int j = 0; j<3; j++){
        labels.at(j)->clear();
    }
}


void user_albom::on_prev_clicked()
{
    if(i>3){

        cleanL();

        i = i - incr;
        int border = i - incr;
        qDebug() << i;
        qDebug() << i;
        int counter = 0;
        for(border; border < i; border++){
            QPixmap outPixmap = QPixmap();
            outPixmap.loadFromData(alboms.at(border).albom_pic);
            labels.at(counter)->setPixmap(outPixmap);
            counter++;
        }
        ui->next->setEnabled(true);
        if(i==3){
            ui->prev->setEnabled(false);
        }
    }
}

void user_albom::on_next_clicked()
{
    if(i<alboms.size()){
        cleanL();
        qDebug() << i;
        int border = i;
        i =  i +  incr;
        int counter = 0;

        for(border; border < i && border < alboms.size();border++){
            QPixmap outPixmap = QPixmap();
            outPixmap.loadFromData(alboms.at(border).albom_pic);
            labels.at(counter)->setPixmap(outPixmap);
            counter++;
        }
        qDebug() << i;
        qDebug() << border;

        ui->prev->setEnabled(true);
        if(border == alboms.size()){
            ui->next->setEnabled(false);
        }
    }
}
