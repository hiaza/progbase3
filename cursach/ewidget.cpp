#include "clipscene.h"
#include "ewidget.h"

#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QGraphicsView>

EWidget::EWidget(QPixmap& x, QWidget* parent) :
    BaseClass(parent)
{
    m_gridLayout = new QGridLayout(this);
    m_graphicsView = new QGraphicsView(this);
    m_gridLayout->addWidget(m_graphicsView), 0, 0;
    //m_pushButton = new QPushButton("Add file", this);
    save_button = new QPushButton("Save changes", this);

    m_clippedLabel = new QLabel(this);
    m_gridLayout->addWidget(m_clippedLabel, 0, 1);
    //m_gridLayout->addWidget(m_pushButton, 1, 0);
    m_gridLayout->addWidget(save_button, 1, 0);
    m_clipScene = new ClipScene(this);
    m_graphicsView->setScene(m_clipScene);

    m_clipScene->setImage(x);
    connect(save_button, &QPushButton::clicked, this, &EWidget::close );
    connect(m_clipScene, &ClipScene::clippedImage, this, &EWidget::onClippedImage);
        resize(640, 480);
}

/*void EWidget::onAddFile()
{
    QString imagePath = QFileDialog::getOpenFileName(this, "Open Image File", QString(), tr("Image (*.png *.jpg)"));
    m_clipScene->setImage(imagePath);
}
*/
void EWidget::onClippedImage(const QPixmap& pixmap)
{
    m_clippedLabel->setPixmap(pixmap);
    cropp = pixmap;
}
