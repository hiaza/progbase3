#ifndef STORAGEMANAGER_H
#define STORAGEMANAGER_H

#include <QObject>

#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QList>

#include "register.h"
#include "user.h"


class storageManager : public QObject
{
    QSqlDatabase db;
    Q_OBJECT
public:
    explicit storageManager(QString path, QObject *parent = 0);

    User * loginUser(LoginData & data);
    bool open();
    void close();
    User * registerUser(RegisterData & data);
    bool addToPicBase(int id,QByteArray pic);

signals:

public slots:
};

#endif // STORAGEMANAGER_H
