#ifndef USER_ALBOM_H
#define USER_ALBOM_H

#include <QDialog>
#include <clickablelabel.h>
#include <recuest.h>
#include <QTcpSocket>
namespace Ui {

class user_albom;
}

class user_albom : public QDialog
{
    Q_OBJECT
    int ID;
    std::vector<albom> alboms;
    QByteArray Data;
public:
    void setAlboms();
    explicit user_albom(std::vector<albom> alboms ,int id,QWidget *parent = 0);
    ~user_albom();

private slots:
    void on_prev_clicked();
    void on_test();
    void on_next_clicked();
    void cleanL();

    void onReadyRead();

    void P_openChoosenPic(recuest * rec);

private:
    std::vector<ClickableLabel*> labels;
    QTcpSocket * clientSocket;
    int i;
    int incr = 3;
    Ui::user_albom *ui;
};

#endif // USER_ALBOM_H
