#include "albomcr.h"
#include "ui_albomcr.h"
#include <QFileDialog>
#include <QBuffer>

albomCr::albomCr(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::albomCr)
{
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
}

albomCr::~albomCr()
{
    delete ui;
}

void albomCr::on_pushButton_clicked()
{
    QString way =  QFileDialog::getOpenFileName(0, "Open Dialog", "", "*.jpg *.png");
    if(way.length()>1){
        QPixmap img (way);
        QBuffer inBuffer( &inByteArray );
        inBuffer.open( QIODevice::WriteOnly);
        img.save( &inBuffer, "JPG");
        ui->label_3->setPixmap(img);
        ui->label_3->setScaledContents(true);
        added = true;
        validate();
     }
}


void albomCr::on_buttonBox_accepted()
{
    albName = ui->lineEdit->text();
}

void albomCr::validate(){
    bool invalidInput = ui -> lineEdit->text().isEmpty()
            || !added;
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(!invalidInput);
}

void albomCr::on_lineEdit_textChanged(const QString &arg1)
{
    validate();
}
