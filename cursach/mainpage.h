#ifndef MAINPAGE_H
#define MAINPAGE_H
#include <clickablelabel.h>
#include <QMainWindow>
#include <QCheckBox>
#include <QTcpSocket>
#include <recuest.h>
#include <QListWidgetItem>

namespace Ui {
class MainPage;
}

class MainPage : public QMainWindow
{
    Q_OBJECT

    QByteArray Data;

    int albCount = 0;
    std::vector<albom> alboms;
    std::vector<bool> forDelete;
    std::vector<QCheckBox*> boxes;
    bool deleteMode = false;
public:

    explicit MainPage(QTcpSocket * client, QWidget *parent = 0);
    void some();
    ~MainPage();
    void setId(int i);
    void setAlboms();
    void prepareChat();

    std::vector<int> count;
    std::vector<ClickableLabel*> labels;
    int i;
    int incr = 3;

private slots:
    void on_pushButton_clicked();
    void on_lineEdit_2_textChanged(const QString &arg1);
    void on_lineEdit_textChanged(const QString &arg1);
    void on_saveButton_clicked();

    void validate();
    void on_createB_clicked();
    void on_test();
    void on_persone();

    void on_prev_clicked();
    void on_next_clicked();
    void cleanL();
    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();
    void on_delete_2_clicked();
    void on_cancel_clicked();
    void on_tabWidget_tabBarClicked(int index);

    void on_delete_3_clicked();
    void on_checkBox_clicked();
    void on_checkBox_2_clicked();
    void on_checkBox_3_clicked();

    void on_changePass_clicked();
    bool validatePassword();
    void validate2();
    void on_pass2_textChanged(const QString &arg1);
    void on_pass3_textChanged(const QString &arg1);
    void on_pass1_textChanged(const QString &arg1);

    void onConnected();
    void onReadyRead();
    void onBytesWritten(qint64 n);
    void onDisconnected();

    void M_setData(recuest * rec);
    void M_setAlboms(recuest * rec);
    void M_openChoosenPic(recuest * rec);
    void M_findUsers (recuest * rec);
    void M_prepareChat ( recuest * rec );
    void M_createAlbom(recuest * rec);
    void M_removeAlboms();
    void M_getFullInformation(recuest*rec);
    void M_getAllUserData(recuest * rec);
    void M_updateUser( recuest * rec);
    void M_getMessages(recuest * rec);
    void M_sendedMessage( recuest * rec);

    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_sendBut_clicked();

    void closeEvent(QCloseEvent*ev);

private:
    Ui::MainPage *ui;
    int ID;
    QTcpSocket * client;
    int currentPressed = -1;
};

#endif // MAINPAGE_H
