#include "mainpage.h"
#include "ui_mainpage.h"
#include <iostream>
#include <QDebug>
#include <QString>
#include <photo.h>
#include <QBuffer>
#include <clickablelabel.h>
#include <QFileDialog>
#include <albomv.h>
#include <albomcr.h>
#include <QLayout>
#include <QMessageBox>
#include <information.h>
#include <page.h>
#include <QCloseEvent>
#include <QCryptographicHash>
#include <QVariant>

using namespace std;

MainPage::MainPage(QTcpSocket * client, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainPage)
{

    this -> client = client;

    connect(client, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(client, SIGNAL(bytesWritten(qint64)), this, SLOT(onBytesWritten(qint64)));
    connect(client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(client, SIGNAL(disconnected()), this, SLOT(onDisconnected()));

    ui->setupUi(this);

    labels.push_back(ui->label_21);
    labels.push_back(ui->label_22);
    labels.push_back(ui->label_23);
    connect(ui->label_21,SIGNAL(clicked()),this,SLOT(on_test()));
    connect(ui->label_22,SIGNAL(clicked()),this,SLOT(on_test()));
    connect(ui->label_23,SIGNAL(clicked()),this,SLOT(on_test()));

    ui->checkBox->hide();
    ui->checkBox_2->hide();
    ui->checkBox_3->hide();
    boxes.push_back(ui->checkBox);
    boxes.push_back(ui->checkBox_2);
    boxes.push_back(ui->checkBox_3);
    ui->cancel->hide();
    ui->delete_3->hide();
}


void MainPage::onConnected() {

    cout << "connected" << endl;

}


void MainPage::onReadyRead() {

    cout << "ready to read" << endl;

    Data.append(client->readAll());

    if(Data.right(6)=="@#%$#@"){

        QString temp = QString::fromStdString(Data.toStdString());

        temp.chop(6);

        recuest * rec;

        rec = rec->deserializeRecuest(temp);

        if (rec->type == RT_GET_INFORMATION){

            M_setData(rec);

        }else if (rec->type == RT_GET_ALBOMS){

            M_setAlboms(rec);

        }else if (rec->type == RT_GET_PHOTOS_FROM_ALBOM){

            M_openChoosenPic(rec);

        }else if (rec->type == RT_FIND_USER){

            M_findUsers(rec);

        }else if (rec->type == RT_GET_PERSONS){

            M_prepareChat(rec);

        }else if (rec->type == RT_ADD_ALBOM){

            M_createAlbom(rec);

        }else if (rec->type == RT_REMOVE_ALBOM){

            M_removeAlboms();

        }else if (rec->type == RT_FULL_INFO){

            M_getFullInformation(rec);

        }else if (rec->type == RT_GET_ALBOMS_AND_DATA){

            M_getAllUserData(rec);

        }else if (rec->type == RT_UPDATE_PHOTO){

            ui->label->setPixmap(rec->p);

            ui->photo->setPixmap(rec->p);

        }else if(rec->type == RT_UPDATE_PASS){

            QMessageBox::information(this,tr("Інфо"),rec->message);

        }else if(rec->type == RT_UPDATE_USER){

            M_updateUser(rec);

        }else if(rec->type == RT_GET_MESSAGES){

            M_getMessages(rec);

        }else if(rec->type == RT_SEND_MESSAGE){

            M_sendedMessage(rec);

        }else if(rec->type == RT_NONE){

            QMessageBox::warning(this,tr("Помилка"),rec->message);

        }
        delete rec;

        cout << "Received:" << endl << Data.toStdString() << endl;

        Data.clear();
    }

}


void MainPage::M_sendedMessage( recuest * rec){


    if((rec->reciever_id == 1)&& (currentPressed == rec->sender_id)){

        QListWidgetItem *itm = new QListWidgetItem(rec->message);

        itm->setTextAlignment(Qt::AlignRight);

        ui->listWidget_2->addItem(itm);

        ui->sendEdit->clear();

    }
    if((rec->reciever_id == 2) && (currentPressed == rec->sender_id)){

        QListWidgetItem *itm = new QListWidgetItem(rec->message);

        itm->setTextAlignment(Qt::AlignLeft);

        ui->listWidget_2->addItem(itm);

        ui->sendEdit->clear();

    }

}

void MainPage::M_updateUser( recuest * rec){

    RData info = rec->data.at(0);

    ui->lineEdit->setText(info.name);

    ui->lineEdit_2->setText(info.surname);

    ui->lineEdit_3->setText(info.city);

    ui->lineEdit_4->setText(info.religion);

    ui->dateEdit->setDate(QDate::fromString(info.birth));

    ui->textEdit->setPlainText(info.hobby);

}

void MainPage::M_prepareChat ( recuest * rec ){

    for(int j = 0; j < (int)rec->data.size(); j++){

        RData user = rec->data.at(j);

        QString str = user.name + " " + user.surname;

        QPixmap outPixmap = QPixmap();

        outPixmap.loadFromData(user.MainPic);

        QIcon x(outPixmap);

        QListWidgetItem *itm = new QListWidgetItem(str);

        QVariant variant;

        variant.setValue(user.id);

        itm->setData(Qt::UserRole,variant);

        itm->setIcon(x);

        ui->listWidget->setIconSize(QSize(48, 48));

        ui->listWidget->addItem(itm);

    }

}


void MainPage::M_findUsers (recuest * rec)
{

        for(int j = 0; j < (int)rec -> data.size(); j++){

            RData user = rec->data.at(j);

            QHBoxLayout * persone = new QHBoxLayout();

            QVBoxLayout * x = new QVBoxLayout();

            QLabel * name_surname = new QLabel();

            name_surname->setText(user.name + " " + user.surname);

            name_surname->setMaximumHeight(150);

            QLabel * pic = new QLabel();

            QPixmap outPixmap = QPixmap();

            outPixmap.loadFromData(user.MainPic);

            pic->setPixmap(outPixmap);

            pic->setMaximumWidth(300);

            pic->setMaximumHeight(300);

            pic->setScaledContents(true);

            ClickableLabel * id = new ClickableLabel();

            id->setMaximumHeight(150);

            id->setText("ID:  " + QString::number(user.id));

            connect(id,SIGNAL(clicked()),this,SLOT(on_persone()));

            x->addWidget(name_surname);

            x->addWidget(id);

            x->minimumSize();

            persone->addWidget(pic);

            persone->addLayout(x);

            persone->minimumSize();

            ui->lay->addLayout(persone);

        }

}

void MainPage::M_getFullInformation(recuest*rec){

    Information p(ID);

    p.set(rec->data.at(0));

    p.exec();

}

void MainPage::M_openChoosenPic(recuest * rec){

    albom te = alboms.at(rec->sender_id);

    AlbomV m(false);

    m.name = te.albom_name;

    m.preparing(rec->photos);

    m.albomId = te.albom_id;

    m.exec();
}


void MainPage::M_setAlboms(recuest * rec){

    alboms.erase(alboms.begin(),alboms.end());

    qDebug() << rec->alboms.size();

    for ( int j = 0; j < (int)rec->alboms.size();j++){

        albom x = rec->alboms.at(j);

        alboms.push_back(x);

        count.push_back(x.albom_id);

        forDelete.push_back(false);

        albCount ++;

    }

    i = 3;

    for(int j = 0; j < i && j<(int)alboms.size();j++){

        QPixmap outPixmap = QPixmap();

        outPixmap.loadFromData(alboms.at(j).albom_pic);

        labels.at(j)->setPixmap(outPixmap);

    }

    if(i < (int)alboms.size()){

        ui->next->setEnabled(true);

    } else ui->next ->setEnabled((false));

    ui->prev->setEnabled(false);

}

void MainPage::M_setData(recuest * rec){

    RData info = rec->data.at(0);

    QPixmap outPixmap = QPixmap();

    outPixmap.loadFromData(info.MainPic);

    ui->label->setPixmap(outPixmap);

    ui->photo->setPixmap(outPixmap);

    ui->nameSur ->setText(info.name + " " + info.surname);

    ui->lineEdit->setText(info.name);

    ui->lineEdit_2->setText(info.surname);

    ui->label_2->setText("City: " + info.city);

    ui->lineEdit_3->setText(info.city);

    ui->lineEdit_4->setText(info.religion);

    ui->dateEdit->setDate(QDate::fromString(info.birth));

    ui->textEdit->setPlainText(info.hobby);

}


void MainPage::M_getMessages(recuest * rec){

    vector<QString>mess;

    ui->listWidget_2->clear();

    ui->sendEdit->clear();

    for(int j = 0; j < (int)rec->alboms.size();j++){

        albom tem = rec->alboms.at(j);

        mess.push_back(tem.albom_name);

    }
    for(int j = 0; j < (int)mess.size() ; j++){

        QString some = mess.at(j);

        bool align = true;

        if(some.at(some.length()-1)=='2'){

            align = false;

        }

        some.chop(1);

        QListWidgetItem *itm = new QListWidgetItem(some);

        if(align){

            itm->setTextAlignment(Qt::AlignRight);

        }else itm->setTextAlignment(Qt::AlignLeft);

        ui->listWidget_2->addItem(itm);

    }
    ui->listWidget_2->scrollToBottom();

}


void MainPage::M_getAllUserData(recuest * rec){

    RData info = rec->data.at(0);

    page open(info,rec->alboms,rec->sender_id);

    QString pageName = "ID"+ QString::number(rec->sender_id) + "   " + info.name + " " + info.surname;

    open.setWindowTitle(pageName);

    open.set();

    this->hide();

    open.exec();

    this->show();

}

void MainPage::M_removeAlboms(){

    int deleted = 0;

    for(int j = 0; j < (int)forDelete.size();j++){

        if(forDelete.at(j)==true){

            alboms.erase(alboms.begin() + j - deleted);

            deleted++;

        }
    }

    forDelete.erase(forDelete.begin(),forDelete.end());

    cleanL();

    setAlboms();

    deleteMode = false;

    ui->delete_2->show();

    ui->delete_2->setEnabled(true);

    ui->createB->show();

    ui->createB->setEnabled(true);

    for(int j = 0; j < (int)boxes.size();j++){

        boxes.at(j)->hide();

        boxes.at(j)->setEnabled(false);

        boxes.at(j)->setChecked(false);

    }

    ui->cancel->hide();

    ui->cancel->setEnabled(false);

    ui->delete_3->hide();

    ui->delete_3->setEnabled(false);

}

void MainPage::onBytesWritten(qint64 n) {

    cout << "bytes  written " << endl;

}

void MainPage::onDisconnected() {

    cout << "disconnected" << endl;
}


void MainPage:: on_persone(){

    ClickableLabel * label = static_cast<ClickableLabel*>(sender());

    int p = label->text().right(4).toInt();

    qDebug() << p;

    recuest * x = new recuest(RT_GET_ALBOMS_AND_DATA, p, -1,vector<albom>(),
                              vector<QPixmap>(),vector<RData>(), LoginData(), "" ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    client->waitForConnected(500);

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    delete x;

}


void MainPage::on_test(){

     ClickableLabel * label = static_cast<ClickableLabel*>(sender());

     int p = ui->horizontalLayout_3->indexOf(label);

     if(i-3+p >= (int)alboms.size()){
         return;
     }

     albom te = alboms.at(i-3+p);

     recuest * x = new recuest(RT_GET_PHOTOS_FROM_ALBOM, i-3+p, te.albom_id,vector<albom>(),
                               vector<QPixmap>(),vector<RData>(), LoginData(), "" ,QPixmap());

     QString requestStr = x->serializeRecuest();

     requestStr = requestStr + "@#%$#@";

     client->waitForConnected(500);

     cout << "Sending: " << endl << requestStr.toStdString() << endl;

     client->write(requestStr.toUtf8());

     client->flush();

     delete x;
}


void MainPage::prepareChat(){

    recuest * x = new recuest(RT_GET_PERSONS, ID,-1,vector<albom>(),vector<QPixmap>(),vector<RData>(), LoginData(), ""  ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    if(!client->waitForReadyRead(1000)) {

        QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

        close();

    }
    delete x;
}


void MainPage:: some(){

    recuest * x = new recuest(RT_GET_INFORMATION, ID,-1,vector<albom>(),vector<QPixmap>(),vector<RData>(), LoginData(), "" ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    client->waitForConnected(500);

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    if(!client->waitForReadyRead(2000)) {

        QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

        close();

    }

    delete x;

}


void MainPage:: setAlboms(){

    recuest * x = new recuest(RT_GET_ALBOMS, ID,-1,vector<albom>(),vector<QPixmap>(),vector<RData>(), LoginData(), "" ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    //client->waitForConnected(500);

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    if(!client->waitForReadyRead(1000)) {

        QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

        close();

    }

    delete x;

}

void MainPage:: setId(int i){

    ID = i;

    some();

}

MainPage::~MainPage()
{
    QString id = QString::number(ID);

    QString requestStr = "{\"type\":-2,\"id\":"+id+"}";

    requestStr = requestStr + "@#%$#@";

    client->write(requestStr.toUtf8());

    client->flush();

    delete client;

    delete ui;

}

void MainPage::on_pushButton_clicked()
{
    QString way =  QFileDialog::getOpenFileName(0, "Open Dialog", "", "*.jpg *.png");

    if(way.length()<2){

        return;

    }

    photo t(way);

    t.exec();

    if(t.saved){
        QPixmap cropped = t.poto;

        QByteArray inByteArray;

        QBuffer inBuffer( &inByteArray );

        inBuffer.open( QIODevice::WriteOnly );

        cropped.save( &inBuffer, "JPG");

        QPixmap original = t.mainPoto;

        QByteArray inByteArray2;

        QBuffer inBuffer2( &inByteArray2 );

        inBuffer2.open( QIODevice::WriteOnly );

        original.save( &inBuffer2, "JPG");

        vector <RData> temp;

        RData pict;

        pict.pic = inByteArray2;

        pict.MainPic = inByteArray;

        temp.push_back(pict);

        recuest * x = new recuest(RT_UPDATE_PHOTO, ID,-1,vector<albom>(),vector<QPixmap>(),temp, LoginData(), "" ,QPixmap());

        QString requestStr = x->serializeRecuest();

        requestStr = requestStr + "@#%$#@";

        cout << "Sending: " << endl << requestStr.toStdString() << endl;

        client->write(requestStr.toUtf8());

        client->flush();

        if(!client->waitForReadyRead(10000)) {

            QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

            close();

        }
    }

}

void MainPage::validate(){

    bool invalidInput = ui -> lineEdit->text().isEmpty()
            || ui->lineEdit_2->text().isEmpty();

    ui->saveButton->setEnabled(!invalidInput);

}

void MainPage::on_lineEdit_textChanged(const QString &arg1)
{

    validate();

}

void MainPage::on_lineEdit_2_textChanged(const QString &arg1)
{

    validate();

}

void MainPage::on_saveButton_clicked()
{
    vector<RData> temp;

    RData u;

    u.name = ui->lineEdit->text();

    u.surname = ui->lineEdit_2->text();

    u.city = ui->lineEdit_3->text();

    u.religion = ui->lineEdit_4->text();

    u.birth = ui->dateEdit->date().toString();

    u.hobby = ui->textEdit->toPlainText();

    temp.push_back(u);

    recuest * x = new recuest(RT_UPDATE_USER, ID,-1,vector<albom>(),vector<QPixmap>(),temp, LoginData(), "" ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    if(!client->waitForReadyRead(1000)) {

        QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

        close();

    }

}

void MainPage::M_createAlbom(recuest * rec){

    albom self = rec->alboms.at(0);

    QPixmap outPixmap = QPixmap();

    outPixmap.loadFromData(self.albom_pic);

    albCount ++;

    alboms.push_back(self);

    forDelete.push_back(false);

    count.push_back(self.albom_id);

    int place = (alboms.size()-1)%3;

    if(i+1>(int)alboms.size()){

        labels.at(place)->setPixmap(outPixmap);

    }else{

        ui->next->setEnabled(true);
    }

}

void MainPage::on_createB_clicked()
{

    albomCr t;

    t.exec();

    vector<albom> coll;

    albom a;

    a.albom_name = t.albName;

    a.albom_pic = t.inByteArray;

    coll.push_back(a);

    recuest * x = new recuest(RT_ADD_ALBOM, ID,-1,coll,vector<QPixmap>(),vector<RData>(), LoginData(), "" ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    if(!client->waitForReadyRead(1000)) {

        QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

        close();

    }

    delete x;

}


void MainPage:: cleanL(){

    for(int j = 0; j<3; j++){

        labels.at(j)->clear();

    }

}

void MainPage::on_prev_clicked()

{
    if(i>3){

        cleanL();

        i = i - incr;

        int border = i - incr;

        int counter = 0;

        for(border; border < i; border++){

            QPixmap outPixmap = QPixmap();

            outPixmap.loadFromData(alboms.at(border).albom_pic);

            labels.at(counter)->setPixmap(outPixmap);

            if(deleteMode){

                boxes.at(counter)->show();

                boxes.at(counter)->setEnabled(true);

                boxes.at(counter)->setChecked(forDelete.at(border));

            }

            counter++;

        }

        ui->next->setEnabled(true);

        if(i==3){

            ui->prev->setEnabled(false);

        }

    }

}

void MainPage::on_next_clicked()
{

    if(i<(int)alboms.size()){

        cleanL();

        qDebug() << i;

        int border = i;

        i =  i +  incr;

        int counter = 0;

        for(border; border < i && border < (int)alboms.size();border++){

            QPixmap outPixmap = QPixmap();

            outPixmap.loadFromData(alboms.at(border).albom_pic);

            labels.at(counter)->setPixmap(outPixmap);

            if(deleteMode){

                boxes.at(counter)->show();

                boxes.at(counter)->setEnabled(true);

                boxes.at(counter)->setChecked(forDelete.at(border));
            }

            counter++;

        }

        ui->prev->setEnabled(true);

        if(border == (int)alboms.size()){

            ui->next->setEnabled(false);

        }

        while(counter!=3 && deleteMode){

            boxes.at(counter) ->hide();

            boxes.at(counter) -> setEnabled(false);

            counter++;

        }
    }
}


static void clearLayout(QLayout *layout)
{

    QLayoutItem *child;

    while ((child = layout->takeAt(0)) != 0) {

        if(child->layout() != 0)

            clearLayout( child->layout() );

        else if(child->widget() != 0)

            delete child->widget();

        delete child;

    }
}

void MainPage::on_pushButton_3_clicked()
{
    clearLayout(ui->lay);

    QString textToFind = ui->search->text();

    recuest * x = new recuest(RT_FIND_USER, ID,-1,vector<albom>(),vector<QPixmap>(),vector<RData>(), LoginData(), textToFind ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    if(!client->waitForReadyRead(1000)) {

        QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

        close();

    }

}


void MainPage::on_pushButton_2_clicked()
{
    recuest * x = new recuest(RT_FULL_INFO, ID,-1,vector<albom>(),vector<QPixmap>(),vector<RData>(), LoginData(), "" ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    if(!client->waitForReadyRead(2000)) {

        QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

        close();

    }


}


void MainPage::on_delete_2_clicked()
{
    deleteMode = true;

    ui->delete_2->hide();

    ui->delete_2->setEnabled(false);

    ui->createB->hide();

    ui->createB->setEnabled(false);

    int counter = i-3;

    while(counter<(int)alboms.size()){

        boxes.at(counter%3)->show();

        boxes.at(counter%3)->setEnabled(true);

        counter++;
    }

    ui->cancel->show();

    ui->cancel->setEnabled(true);

    ui->delete_3->show();

    ui->delete_3->setEnabled(true);

}

void MainPage::on_cancel_clicked()
{
    deleteMode = false;

    ui->delete_2->show();

    ui->delete_2->setEnabled(true);

    ui->createB->show();

    ui->createB->setEnabled(true);

    ui->checkBox->hide();

    ui->checkBox_2->hide();

    ui->checkBox_3->hide();

    ui->checkBox->setEnabled(false);

    ui->checkBox_2->setEnabled(false);

    ui->checkBox_3->setEnabled(false);

    ui->cancel->hide();

    ui->cancel->setEnabled(false);

    ui->delete_3->hide();

    ui->delete_3->setEnabled(false);
}


void MainPage::on_tabWidget_tabBarClicked(int index)
{
    clearLayout(ui->lay);
}


void MainPage::on_delete_3_clicked()
{

    vector<albom>forErase;

    for(int j = 0; j < (int)forDelete.size();j++){

        if(forDelete.at(j)==true){

            albom temp;

            temp.albom_id = alboms.at(j).albom_id;

            forErase.push_back(temp);

         }
    }

    recuest * x = new recuest(RT_REMOVE_ALBOM, ID,-1, forErase ,vector<QPixmap>(),vector<RData>(), LoginData(), "" ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    if(!client->waitForReadyRead(5000)) {

        QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

        close();

    }

}

void MainPage::on_checkBox_clicked()
{

    if(i-3<(int)forDelete.size()){

        if(forDelete.at(i-3)==false){

            forDelete.at(i-3) = true;

        }else forDelete.at(i-3) = false;
    }

}

void MainPage::on_checkBox_2_clicked()
{

    if(i-2<(int)forDelete.size()){

        if(!forDelete.at(i-2)){

            forDelete.at(i-2) = true;

        } else forDelete.at(i-2) = false;

    }

}


void MainPage::on_checkBox_3_clicked()
{

    if(i-1<(int)forDelete.size()){

        if(!forDelete.at(i-1)){

            forDelete.at(i-1) = true;

        }else forDelete.at(i-1) = false;

    }
}

void MainPage::on_changePass_clicked()
{
    QByteArray hashArray = QCryptographicHash::hash(ui->pass1->text().toUtf8(), QCryptographicHash::Md5);

    QString pass = QString(hashArray.toHex());

    LoginData tmp;

    QByteArray hashArray2 = QCryptographicHash::hash(ui->pass2->text().toUtf8(), QCryptographicHash::Md5);

    tmp.login = QString(hashArray2.toHex());

    recuest * x = new recuest(RT_UPDATE_PASS, ID, -1, vector<albom>() ,vector<QPixmap>(),vector<RData>(),tmp, pass ,QPixmap());

    QString requestStr = x->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    client->write(requestStr.toUtf8());

    client->flush();

    if(!client->waitForReadyRead(5000)) {

        QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

        close();
    }
}


void MainPage::validate2(){

    bool invalid_password = validatePassword();

    bool invalidInput = ui -> pass1->text().isEmpty()
            || ui->pass2->text().isEmpty()
            || ui->pass3->text().isEmpty()
            || ui->pass1->text().compare(ui->pass2->text())==0
            || invalid_password;

    ui->changePass->setEnabled(!invalidInput);

}

bool MainPage::validatePassword(){
    bool invalidInput = ui -> pass2->text().isEmpty()

            || ui->pass3->text().isEmpty()

            || ui->pass2->text().compare(ui->pass3->text())!=0;

    return invalidInput;
}


void MainPage::on_pass2_textChanged(const QString &arg1)
{

    validate2();

}

void MainPage::on_pass3_textChanged(const QString &arg1)
{

    validate2();

}

void MainPage::on_pass1_textChanged(const QString &arg1)
{

    validate2();

}

void MainPage::on_listWidget_itemClicked(QListWidgetItem *item)
{

    QList<QListWidgetItem*> selectedItems = ui->listWidget->selectedItems();

    if(selectedItems.count() == 1){

        QVariant variant = item->data(Qt::UserRole);

        int s = variant.value<int>();

        currentPressed = s;

        recuest * x = new recuest(RT_GET_MESSAGES, ID, s, vector<albom>(),vector<QPixmap>(),vector<RData>(), LoginData(), "" ,QPixmap());

        QString requestStr = x->serializeRecuest();

        requestStr = requestStr + "@#%$#@";

        cout << "Sending: " << endl << requestStr.toStdString() << endl;

        client->write(requestStr.toUtf8());

        client->flush();

        if(!client->waitForReadyRead(1000)) {

            QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

            close();

        }
        ui->sendBut->setEnabled(true);
        ui->sendEdit->setEnabled(true);
    }
    else
    {
        ui->sendBut->setEnabled(false);
        ui->sendEdit->setEnabled(false);
    }
}

void MainPage::on_sendBut_clicked()
{
    QList<QListWidgetItem*> selectedItems = ui->listWidget->selectedItems();

    if(selectedItems.count() == 1  && !(ui->sendEdit->text().isEmpty())){

        QListWidgetItem * item = selectedItems.at(0);

        QVariant variant = item->data(Qt::UserRole);

        int s = variant.value<int>();

        recuest * x = new recuest(RT_SEND_MESSAGE, ID, s, vector<albom>(),vector<QPixmap>(),vector<RData>(), LoginData(), ui->sendEdit->text(),QPixmap());

        QString requestStr = x->serializeRecuest();

        requestStr = requestStr + "@#%$#@";

        cout << "Sending: " << endl << requestStr.toStdString() << endl;

        client->write(requestStr.toUtf8());

        client->flush();

        if(!client->waitForReadyRead(1000)) {

            QMessageBox::critical(this, tr("ERROR"), tr("Connection ERROR"));

            close();

        }
    }
}

void MainPage::closeEvent(QCloseEvent*ev){
    int res = QMessageBox::question(this,
                                                            "Exit","Are you sure?",
                                                            QMessageBox::Yes |QMessageBox::No,
                                                            QMessageBox::No);
    if (res == QMessageBox::Yes){
        ev->accept();
    }
    else {
        ev->ignore();
    }
}

