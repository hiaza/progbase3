#ifndef ALBOMCR_H
#define ALBOMCR_H

#include <QDialog>

namespace Ui {
class albomCr;
}

class albomCr : public QDialog
{
    Q_OBJECT

public:
    explicit albomCr(QWidget *parent = 0);
    ~albomCr();
    QByteArray inByteArray;
    QString albName;
    bool added = false;
private slots:
    void on_pushButton_clicked();

    void validate();

    void on_buttonBox_accepted();

    void on_lineEdit_textChanged(const QString &arg1);

private:
    Ui::albomCr *ui;
};

#endif // ALBOMCR_H
