#include "mainpage.h"
#include "login.h"
#include <QApplication>

#include <QTcpSocket>
int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    QTcpSocket * client = new QTcpSocket();

    client->connectToHost("127.0.0.1",3000);

    login authorization(client);
    int i = authorization.exec();
    while(i != authorization.Rejected){
        if(authorization.status){
            int id = authorization.ID;
            authorization.hide();

            QString ID = QString::number(id);
            QString requestStr = "{\"type\":-1,\"id\":"+ID+"}";
            requestStr = requestStr + "@#%$#@";
            client->write(requestStr.toUtf8());
            client->flush();

            MainPage w(client);
            w.setId(id);
            client->waitForReadyRead(1000);
            w.prepareChat();
            client->waitForReadyRead(1000);
            w.setAlboms();
            w.show();
            a.exec();

            break;
        }
        i = authorization.exec();
    }

    return 0;
}
