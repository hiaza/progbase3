#include "photo.h"
#include "ui_photo.h"

#include <QFileDialog>
#include <QBuffer>
#include <photo.h>
#include "clipscene.h"


#include <QtWidgets/QGridLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QGraphicsView>

photo::photo(QString way, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::photo)
{

    ui->setupUi(this);
    saved = false;

        QPixmap img (way);
         m_gridLayout = new QGridLayout(this);
         m_graphicsView = new QGraphicsView(this);
         m_gridLayout->addWidget(m_graphicsView), 0, 0;
         //m_pushButton = new QPushButton("Add file", this);
         save_button = new QPushButton("Save changes", this);

         m_clippedLabel = new QLabel(this);
         m_gridLayout->addWidget(m_clippedLabel, 0, 1);
         //m_gridLayout->addWidget(m_pushButton, 1, 0);
         m_gridLayout->addWidget(save_button, 1, 0);
         m_clipScene = new ClipScene(this);
         m_graphicsView->setScene(m_clipScene);
         mainPoto = img;
         m_clipScene->setImage(img);
         connect(save_button, &QPushButton::clicked, this, &photo::clickedOnSave);
         connect(m_clipScene, &ClipScene::clippedImage, this, &photo::onClippedImage);
         resize(640, 480);



}

photo::~photo()
{
    delete ui;
}

void photo::clickedOnSave(){
    saved = true;
    this->close();
}

void photo::onClippedImage(const QPixmap& pixmap)
{
    m_clippedLabel->setPixmap(pixmap);
    poto = pixmap;
}
