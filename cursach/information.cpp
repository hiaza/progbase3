#include "information.h"
#include "ui_information.h"
#include <QDebug>

Information::Information(int id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Information)
{
    ui->setupUi(this);
    ID = id;
}

void Information:: set(RData temp){
        ui->label_2->setText(temp.name);
        ui->label_4->setText(temp.surname);
        ui->label_10->setText(temp.city);
        ui->label_8->setText(temp.religion);
        ui->label_6->setText(temp.birth);
        ui->textEdit->setPlainText(temp.hobby);
}

Information::~Information()
{
    delete ui;
}
