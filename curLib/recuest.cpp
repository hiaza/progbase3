#include "recuest.h"
#include <QObject>
#include <QBuffer>
recuest:: recuest(QObject *parent) : QObject(parent)
{

}

recuest:: recuest(recuestType type, int sender_id, int reciever_id,std::vector<albom>alboms,
                  std::vector<QPixmap>photos,
                  std::vector<RData>data,
                  LoginData logData, QString message,  QPixmap p){
    this->type = type;
    this->sender_id = sender_id;
    this->reciever_id = reciever_id;
    this->alboms = alboms;
    this->photos = photos;
    this->data = data;
    this->logData = logData;
    this->message = message;
    this->p = p;
}

recuest::~recuest(){

}



static QJsonValue jValFromPixmap(const QPixmap &p){

    QBuffer buffer;

    buffer.open(QIODevice::WriteOnly);

    p.save(&buffer,"JPG");

    auto const encoded = buffer.data().toBase64();

    return {QLatin1String(encoded)};

}

static QPixmap pixmapFromJVal(const QJsonValue & val){

    auto const encoded = val.toString().toLatin1();

    QPixmap m;

    m.loadFromData(QByteArray::fromBase64(encoded));

    return m;
}

QString recuest:: serializeRecuest(){

    QJsonDocument doc;

    QJsonObject writersObj;

    QJsonArray albomsArr;

    QJsonArray picArr;

    QJsonArray dataArr;

    writersObj.insert("type", this->type);

    writersObj.insert("sen_id", this->sender_id);

    writersObj.insert("p", jValFromPixmap(this->p));

    if(this->reciever_id!=-1){

        writersObj.insert("rec_id", this->reciever_id);

    }

    if(!this->alboms.empty()){

        for (auto & m: this->alboms) {

            QJsonObject mo;

            mo.insert("albom_id", m.albom_id);

            mo.insert("albom_name", m.albom_name);

            mo.insert("albom_pic",  QLatin1String(m.albom_pic.toBase64()));

            albomsArr.append(mo);

        }

        writersObj.insert("alboms", albomsArr);

    }

    if(!this->photos.empty()){

        for (auto & m: this->photos) {

            QJsonObject mo;

            mo.insert("pic",  jValFromPixmap(m));

            picArr.append(mo);

        }

        writersObj.insert("pictures", picArr);
    }

    if( ! this->data.empty()){

        for (auto & m: this->data) {

            QJsonObject mo;

            mo.insert("per_id",m.id);

            mo.insert("name", m.name);

            mo.insert("surname", m.surname);

            mo.insert("city", m.city);

            mo.insert("hobby", m.hobby);

            mo.insert("religion", m.religion);

            mo.insert("birth", m.birth);

            mo.insert("pic",  QLatin1String(m.pic.toBase64()));

            mo.insert("mainPic",  QLatin1String(m.MainPic.toBase64()));

            dataArr.append(mo);

        }

        writersObj.insert("data", dataArr);

    }

    if(this->logData.login!=""){

        QJsonObject mo;

        mo.insert("login", this->logData.login);

        mo.insert("pass", this->logData.password);

        writersObj.insert("log_and_pass", mo);
     }

    if(this->message.length()>1){

        writersObj.insert("mess", this->message);
     }

    doc.setObject(writersObj);

    QString temp = QString::fromStdString(doc.toJson().toStdString());

    return temp;

}

recuest * recuest::deserializeRecuest(QString j){

    QJsonParseError err;

    QJsonDocument doc = QJsonDocument::fromJson(
                QByteArray::fromStdString(j.toStdString()),
                &err);

    if (err.error != QJsonParseError::NoError) {

        return NULL;

    }

    recuest * m = new recuest();

    m->type = (recuestType)doc.object().value("type").toInt();

    m->sender_id = (recuestType)doc.object().value("sen_id").toInt();

    if(doc.object().contains("rec_id")){

        m->reciever_id = doc.object().value("rec_id").toInt();

    } else m->reciever_id = -1;

    if(doc.object().contains("mess")){

        m->message = doc.object().value("mess").toString();

    } else m->message= "";

    if(doc.object().contains("p")){

        m->p = pixmapFromJVal(doc.object().value("p"));

    } else m->p = QPixmap();

    if(doc.object().contains("alboms")){

        QJsonArray writersArr = doc.object().value("alboms").toArray();

        for (int i = 0; i < writersArr.size(); i++) {

                  QJsonValue value = writersArr.at(i);

                  QJsonObject writerObj = value.toObject();

                  albom g;

                  g.albom_id = writerObj.value("albom_id").toInt();

                  g.albom_name = writerObj.value("albom_name").toString();

                  g.albom_pic = QByteArray::fromBase64(writerObj.value("albom_pic").toString().toLatin1());

                  m->alboms.push_back(g);

            }

    } else m->alboms = std:: vector <albom> ();


//    if(doc.object().contains("pictures")){

//        QJsonArray writersArr = doc.object().value("pictures").toArray();

//        for (int i = 0; i < writersArr.size(); i++) {

//                  QJsonValue value = writersArr.at(i);

//                  QJsonObject writerObj = value.toObject();

//                  QPixmap photo = pixmapFromJVal(writerObj.value("pic"));

//                  m->photos.push_back(photo);

//            }

//    } else m->photos = std:: vector <QPixmap> ();

    if(doc.object().contains("pictures")){

        QJsonArray writersArr = doc.object().value("pictures").toArray();

        for (int i = 0; i < writersArr.size(); i++) {

                  QJsonValue value = writersArr.at(i);

                  QJsonObject writerObj = value.toObject();

                  QPixmap photo = pixmapFromJVal(writerObj.value("pic"));

                  m->photos.push_back(photo);

            }

    } else m->photos = std:: vector <QPixmap> ();

    if(doc.object().contains("data")){

        QJsonArray writersArr = doc.object().value("data").toArray();

        for (int i = 0; i < writersArr.size(); i++) {

                  QJsonValue value = writersArr.at(i);

                  QJsonObject writerObj = value.toObject();

                  RData g;

                  g.birth = writerObj.value("birth").toString();

                  g.city = writerObj.value("city").toString();

                  g.hobby = writerObj.value("hobby").toString();

                  g.id = writerObj.value("per_id").toInt();

                  g.name = writerObj.value("name").toString();

                  g.surname = writerObj.value("surname").toString();

                  g.religion = writerObj.value("religion").toString();

                  g.pic = QByteArray::fromBase64(writerObj.value("pic").toString().toLatin1());

                  g.MainPic = QByteArray::fromBase64(writerObj.value("mainPic").toString().toLatin1());

                  m->data.push_back(g);

            }

    } else m->data = std::vector <RData> ();

    if(doc.object().contains("log_and_pass")){

        QJsonObject writerObj = doc.object().value("log_and_pass").toObject();

        LoginData g;

        g.login = writerObj.value("login").toString();

        g.password = writerObj.value("pass").toString();

        m->logData = g;

    } else m->logData = LoginData();

    return m;

}
