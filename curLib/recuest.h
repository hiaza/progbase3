#ifndef RECUEST_H
#define RECUEST_H

#include <QObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QPixmap>
#include <user.h>
#include "curlib_global.h"
typedef struct albom{
    int albom_id;
    QString albom_name;
    QByteArray albom_pic;
}albom;


struct RData {
    int id;
    QString name;
    QString surname;
    QString city;
    QString hobby;
    QString religion;
    QString birth;
    QByteArray pic;
    QByteArray MainPic;
};


typedef enum {
    RT_NONE,
    RT_OPEN_DB,
    RT_LOGIN,
    RT_CHEAK_LOGIN,
    RT_DISCONNECT,
    RT_ADD_USER,
    RT_ADD_ALBOM,
    RT_ADD_PIC_TO_ALBOM,

    RT_GET_INFORMATION,
    RT_GET_ALBOMS,
    RT_GET_USERS,
    RT_GET_MESSAGES,
    RT_GET_PHOTOS_FROM_ALBOM,
    RT_GET_INFORMATIONS_ALL_PHOTOS_AND_ALBOMS,

    RT_UPDATE_USER,
    RT_UPDATE_PASS,
    RT_FIND_USER,

    RT_GET_PERSONS,
    RT_SEND_MESSAGE,
    RT_RECIEVE_MESSAGE,
    RT_REMOVE_ALBOM,
    RT_FULL_INFO,
    RT_GET_ALBOMS_AND_DATA,
    RT_UPDATE_PHOTO,
    RT_RECOVER_PASS,
    RT_RECOVER_PASS2,
    RT_RECOVER_PASS3
}recuestType;



class CURLIBSHARED_EXPORT recuest : public QObject
{

    Q_OBJECT

public:

    recuestType type;
    int sender_id;
    int reciever_id;
    std::vector<albom>alboms;
    std::vector<QPixmap>photos;
    std::vector<RData>data;
    LoginData logData;
    QString message;
    QPixmap p;
    explicit recuest(QObject *parent = 0);
    recuest(recuestType type, int sender_id, int reciever_id, std::vector<albom>alboms,
            std::vector<QPixmap>photos,
            std::vector<RData>data,
            LoginData logData, QString message, QPixmap p);
    ~recuest();
    QString serializeRecuest();
    recuest * deserializeRecuest(QString j);

signals:

public slots:
};

#endif // RECUEST_H
