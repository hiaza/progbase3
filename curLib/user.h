#ifndef USER_H
#define USER_H

#include "curlib_global.h"
#include <QObject>

struct LoginData {
    QString login;
    QString password;
};

enum UserRole {
    SimpleUser,
    Admin
};


class CURLIBSHARED_EXPORT User : public QObject
{
    Q_OBJECT
public:
    explicit User(QObject *parent = 0);

    int id;
    QString login;
    UserRole role;

signals:

public slots:
};

#endif // USER_H
