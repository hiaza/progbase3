#ifndef CURLIB_GLOBAL_H
#define CURLIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CURLIB_LIBRARY)
#  define CURLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CURLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CURLIB_GLOBAL_H
