#-------------------------------------------------
#
# Project created by QtCreator 2018-06-10T18:22:52
#
#-------------------------------------------------

QT       += gui

TARGET = curLib
TEMPLATE = lib

CONFIG +=c++11

DEFINES += CURLIB_LIBRARY

SOURCES += \
    user.cpp \
    recuest.cpp

HEADERS +=\
        curlib_global.h \
    user.h \
    recuest.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
