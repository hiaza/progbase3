#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <storagemanager.h>
namespace Ui {
class MainWindow;
}

typedef struct client{

    QTcpSocket * reciever;

    int id;

}client;

class MainWindow : public QMainWindow

{
    Q_OBJECT

    QTcpServer * tcpServer;

    QByteArray Data;

    std::vector<client> clients;

public:

    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

public slots:

    void onNewConnection();

    void onReadyRead();

    void onBytesWritten(qint64 n);

    void onClientDisconnected();

private slots:

    void addNewUser(recuest * rec, QTcpSocket * clientSocket);

    void checkUser(recuest * rec, QTcpSocket * clientSocket);

    void getUsers(recuest * rec, QTcpSocket * clientSocket);

    void getInformation( recuest * rec, QTcpSocket * clientSocket);

    void getAlboms( recuest * rec, QTcpSocket * clientSocket);

    void getPictures( recuest * rec, QTcpSocket * clientSocket);

    void findUsers( recuest * rec, QTcpSocket * clientSocket);

    void prepareChat( recuest * rec, QTcpSocket * clientSocket);

    void addAlbom( recuest * rec, QTcpSocket * clientSocket);

    void removeAlboms(recuest * rec, QTcpSocket * clientSocket);

    void getFullInfo(recuest * rec , QTcpSocket * clientSocket);

    void getAlbomsAndInfo(recuest * rec , QTcpSocket * clientSocket);

    void addPhoto(recuest * rec, QTcpSocket * clientSocket);

    void updatePhoto(recuest * rec , QTcpSocket * clientSocket);

    void updatePass(recuest * rec, QTcpSocket * clientSocket);

    void updateInformation(recuest * rec, QTcpSocket * clientSocket);

    void getMessages(recuest*rec,QTcpSocket * clientSocket);

    void sendMessage(recuest*rec,QTcpSocket * clientSocket);

    void compareLogin(recuest * rec, QTcpSocket * clientSocket);

    void compareAnswer(recuest * rec, QTcpSocket * clientSocket);

    void recoverPass(recuest * rec, QTcpSocket * clientSocket);

protected:

private:

    Ui::MainWindow *ui;

    storageManager * storage;

};

#endif // MAINWINDOW_H
