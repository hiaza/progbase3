#-------------------------------------------------
#
# Project created by QtCreator 2018-06-05T05:11:11
#
#-------------------------------------------------

QT       += core gui network sql
CONFIG += console

CONFIG +=c++11
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = serv
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    storagemanager.cpp

HEADERS  += mainwindow.h\
    storagemanager.h

FORMS    += mainwindow.ui

INCLUDEPATH += ../curLib
LIBS += -L ../build-curLib-Desktop-Debug -lcurLib
