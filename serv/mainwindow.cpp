#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <QTcpSocket>
#include <QDir>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <recuest.h>
#include <user.h>
#include <QDir>
#include <QBuffer>
#include <QCryptographicHash>

using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    tcpServer = new QTcpServer(this);
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(onNewConnection()));

    int PORT = 3000;
    if (!tcpServer->listen(QHostAddress::Any, PORT)) {
        cerr << "error listen " << endl;
    } else {
        cout << "started at " << PORT << endl;
    }

    QString Path = QDir::currentPath() + "/Logs.sqlite3";
    qDebug() << Path;
    storage = new storageManager(Path);
        if (!storage->open()) {
            // @todo error
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow:: onNewConnection() {
    cout << "got new connection" << endl;
    QTcpSocket * clientSocket = tcpServer->nextPendingConnection();

    connect(clientSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(clientSocket, SIGNAL(bytesWritten(qint64)), this, SLOT(onBytesWritten(qint64)));
    connect(clientSocket, SIGNAL(disconnected()), this, SLOT(onClientDisconnected()));
}

void MainWindow::onReadyRead() {

    QTcpSocket * clientSocket = static_cast<QTcpSocket*>(sender());

    cout << "receiving data" << endl;

    Data.append(clientSocket->readAll());

    if(Data.right(6)=="@#%$#@"){

        QString temp = QString::fromStdString(Data.toStdString());

        temp.chop(6);

        QByteArray dat = QByteArray::fromStdString(temp.toStdString());

        QJsonParseError docError;

        QJsonDocument doc = QJsonDocument::fromJson(dat, &docError);

        cout << "Received:" << endl << dat.toStdString() << endl;

        if(docError.errorString().toInt()==QJsonParseError::NoError){

            recuest * rec;

            rec = rec->deserializeRecuest(temp);

            if(doc.object().value("type").toInt()==-1){

                client temp;
                temp.reciever = clientSocket;
                temp.id = doc.object().value("id").toInt();
                qDebug() << temp.id;
                clients.push_back(temp);
                qDebug() << clients.size() << " - online now" << endl;
            }

            else if(doc.object().value("type").toInt()== -2){
                    int id = doc.object().value("id").toInt();
                    int counter = 0;
                    for(int i = 0; i< clients.size();i++){

                        if(clients.at(i).id == id){
                            counter = i;
                            break;
                        }
             }
                    qDebug() << id << " - go offline from uncanon" << endl;
                    clients.erase(clients.begin()+counter);
                    qDebug() << clients.size() << " - online now" << endl;
                    clientSocket->close();
            }

            else if ( rec->type == RT_SEND_MESSAGE){

                sendMessage(rec,clientSocket);

            }else if (rec->type == RT_CHEAK_LOGIN){

                checkUser(rec, clientSocket);

            }else if (rec->type == RT_ADD_USER){

                addNewUser(rec, clientSocket);

            }else if (rec->type == RT_GET_USERS){

                getUsers(rec,clientSocket);

            }else if(rec->type == RT_GET_INFORMATION){

                getInformation(rec,clientSocket);

            }else if(rec->type == RT_GET_ALBOMS){

                getAlboms(rec,clientSocket);

            }else if(rec->type == RT_GET_PHOTOS_FROM_ALBOM){

                getPictures(rec,clientSocket);

            }else if(rec->type == RT_GET_PERSONS){

                prepareChat(rec,clientSocket);

            }else if(rec->type == RT_ADD_ALBOM){

                addAlbom(rec,clientSocket);

            }else if(rec->type == RT_REMOVE_ALBOM){

                removeAlboms(rec,clientSocket);

            }else if(rec->type == RT_FULL_INFO){

                getFullInfo(rec,clientSocket);

            }else if(rec->type == RT_GET_ALBOMS_AND_DATA){

                getAlbomsAndInfo(rec ,clientSocket);

            }else if(rec->type == RT_ADD_PIC_TO_ALBOM){

                addPhoto(rec ,clientSocket);

            }else if(rec->type == RT_UPDATE_PHOTO){

                updatePhoto(rec ,clientSocket);

            }else if(rec->type == RT_UPDATE_PASS){

                updatePass(rec ,clientSocket);

            }else if(rec->type == RT_FIND_USER){

                findUsers(rec,clientSocket);

            }else if(rec->type == RT_UPDATE_USER){

                updateInformation(rec,clientSocket);

            }else if(rec->type == RT_RECOVER_PASS){

                compareLogin(rec,clientSocket);

            }else if(rec->type == RT_RECOVER_PASS2){

                compareAnswer(rec,clientSocket);

            }else if(rec->type == RT_RECOVER_PASS3){

                recoverPass(rec,clientSocket);

            }else if(rec->type == RT_GET_MESSAGES){

                getMessages(rec,clientSocket);

            }
        }
         Data.clear();
    }

}

void MainWindow::recoverPass(recuest * rec, QTcpSocket * clientSocket){

    QString id = QString::number(rec->sender_id);

    QSqlQuery query;

    QByteArray hashArray = QCryptographicHash::hash(rec->message.toUtf8(), QCryptographicHash::Md5);

    QString passHash = QString(hashArray.toHex());

    query.prepare("UPDATE users SET password_hash = ? WHERE id = ?");

    query.addBindValue(passHash);

    query.addBindValue(id);

    bool b = query.exec();

    int result = 1;

    if (!b) {
        qDebug() << "не вдається створити таблицю";
        result = -1;
    }

    recuest * err;

    err = new  recuest(RT_RECOVER_PASS3, rec->sender_id, result, vector<albom>(),
                               vector<QPixmap>(),vector<RData>(), LoginData(),
                               "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();
}

void MainWindow::compareAnswer(recuest * rec, QTcpSocket * clientSocket){

    QString id = QString::number(rec->sender_id);

    QSqlQuery query;

    query.prepare("SELECT answer FROM users WHERE id = ?");

    query.addBindValue(id);

    bool b = query.exec();

    if (!b) {
        qDebug() << "не вдається створити таблицю";
    }

    QByteArray hashArray = QCryptographicHash::hash(rec->message.toUtf8(), QCryptographicHash::Md5);

    QString answerHash = QString(hashArray.toHex());

    QString answer = "";

    while(query.next()){

        answer = query.value(0).toString();

    }
    int result = -1;

    if(answerHash.compare(answer)==0){

        result = 1;

    }

    recuest * err;

    err = new  recuest(RT_RECOVER_PASS2, rec->sender_id, result, vector<albom>(),
                               vector<QPixmap>(),vector<RData>(), LoginData(),
                               "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();
}

void MainWindow::compareLogin(recuest * rec, QTcpSocket * clientSocket){

    QString login = rec->logData.login;

    QSqlQuery query;

    query.prepare("SELECT id, question FROM users WHERE login = ?");

    query.addBindValue(login);

    bool b = query.exec();

    if (!b) {
        qDebug() << "не вдається створити таблицю";
    }

    LoginData temp;

    int id = -1;

    while(query.next()){

        temp.login = query.value(1).toString();

        id = query.value(0).toInt();

    }

    recuest * err;

    err = new  recuest(RT_RECOVER_PASS, id, -1, vector<albom>(),
                               vector<QPixmap>(),vector<RData>(), temp,
                               "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::sendMessage(recuest*rec,QTcpSocket * clientSocket){

    int id = rec->sender_id;

    QString ID = QString::number(id);
    QString recieverID = QString::number(rec->reciever_id);

    QString messForSender = rec->message + "1";
    QString messForReciever = rec->message + "2";

    QTcpSocket * reciever;

    bool online = false;

    for(int i = 0; i < clients.size();i++){

        if(clients.at(i).id == rec->reciever_id){

            online = true;

            reciever = clients.at(i).reciever;

            break;
        }

    }
    QString forVal = ":id" + recieverID;

    QString forVal2 = ":id" + ID;

    QSqlQuery query;

    QString sendQuery = "INSERT INTO user_" + ID + " ( id" + recieverID +" ) VALUES (" + forVal + ")";

    query.prepare(sendQuery);

    query.bindValue(forVal,messForSender);

    qDebug()<<sendQuery;

    query.exec();

    query.clear();

    QString recQuery = "INSERT INTO user_" + recieverID + " ( id" + ID +" ) VALUES ( " + forVal2 + ")";

    query.prepare(recQuery);

    query.bindValue(forVal2,messForReciever);

    qDebug()<<recQuery;

    query.exec();

    if(online){

        recuest * err;

        err = new recuest(RT_SEND_MESSAGE, rec->sender_id, 2, vector<albom>(),
                                   vector<QPixmap>(), vector<RData>(), LoginData(),
                                   rec->message ,QPixmap());

        QString requestStr = err->serializeRecuest();

        requestStr = requestStr + "@#%$#@";

        cout << "Sending: " << endl << requestStr.toStdString() << endl;

        reciever->write(requestStr.toUtf8());

        reciever->flush();

    }
    recuest * err;

    err = new recuest(RT_SEND_MESSAGE, rec->reciever_id, 1, vector<albom>(),
                               vector<QPixmap>(), vector<RData>(), LoginData(),
                               rec->message ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::getMessages(recuest*rec,QTcpSocket * clientSocket){

    QSqlQuery query;

    vector<albom> temp;

    QString home = "user_" + QString::number(rec->sender_id);

    QString guest = "id" + QString::number(rec->reciever_id);

    QString recue = "SELECT " + guest + " FROM " + home;

    query.prepare(recue);

    query.exec();

    while(query.next()){
        if(!query.value(0).isNull()){
            albom rr;
            rr.albom_name = query.value(0).toString();
            temp.push_back(rr);

        }
    }

    recuest * err;

    err = new recuest(RT_GET_MESSAGES, rec->sender_id, rec->reciever_id, temp,
                               vector<QPixmap>(), vector<RData>(), LoginData(),
                               "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::addPhoto(recuest * rec, QTcpSocket * clientSocket){

    QSqlQuery query;

    QByteArray inByteArray;

    QBuffer inBuffer( &inByteArray );

    inBuffer.open( QIODevice::WriteOnly);

    rec->p.save( &inBuffer, "JPG");

    query.prepare("INSERT INTO user_17_pic (pic,albom_id) VALUES (:pic, :albom_id)");

    query.bindValue(":pic", inByteArray );

    query.bindValue(":albom_id", QString::number(rec->reciever_id));

    if (!query.exec()) {
        return;
    }

    recuest * err;

    err = new recuest(RT_ADD_PIC_TO_ALBOM, rec->sender_id, rec->reciever_id, vector<albom>(),
                               vector<QPixmap>(), vector<RData>(), LoginData(),
                               "" ,rec->p);

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}


void MainWindow::prepareChat(recuest * rec, QTcpSocket * clientSocket){

    QSqlQuery query;

    QString id = QString::number(rec->sender_id);

    query.prepare("SELECT id,name,surname,mainpic FROM users_data WHERE id != ?");

    query.addBindValue(id);

    bool b = query.exec();

    if (!b) {

        qDebug() << "не вдається створити таблицю";

    }

    vector<RData> users;

    while(query.next()){

        RData user;

        user.id = query.value(0).toInt();

        user.name = query.value(1).toString();

        user.surname = query.value(2).toString();

        user.MainPic = query.value(3).toByteArray();

        users.push_back(user);

    }

    recuest * err;

    err = new recuest(RT_GET_PERSONS, rec->sender_id, rec->reciever_id, vector<albom>(),
                               vector<QPixmap>(), users, LoginData(),
                               "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::addAlbom( recuest * rec, QTcpSocket * clientSocket){


    albom temp = rec->alboms.at(0);

    QSqlQuery query;

    QString id = QString::number(rec->sender_id);

    query.prepare("INSERT INTO alboms (albom_name, albom_pic, user_id) VALUES (:albom_name, :albom_pic, :id)");

    query.bindValue(":albom_name", temp.albom_name);

    query.bindValue(":albom_pic", temp.albom_pic);

    query.bindValue(":id", id);

    bool b = query.exec();

    if (!b) {

        qDebug() << "не вдається створити таблицю";
        return;

    }

    temp.albom_id = query.lastInsertId().value<int>();

    vector<albom> r;

    r.push_back(temp);

    recuest * err;

    err = new recuest(RT_ADD_ALBOM, rec->sender_id, rec->reciever_id, r,
                               vector<QPixmap>(), vector<RData>(), LoginData(),
                               "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::findUsers( recuest * rec, QTcpSocket * clientSocket){

    QSqlQuery query;

    QString id = QString::number(rec->sender_id);

    query.prepare("SELECT name,surname,mainpic, id FROM users_data WHERE id != ?");

    query.addBindValue(id);

    bool b = query.exec();

    if (!b) {

        qDebug() << "не вдається створити таблицю";

    }

    vector<RData> users;

    while(query.next()){

        QString str = query.value(0).toString() + " " + query.value(1).toString();

        if(str.contains(rec->message)){

            RData user;

            user.name = query.value(0).toString();

            user.surname = query.value(1).toString();

            user.MainPic = query.value(2).toByteArray();

            user.id = query.value(3).toInt();

            users.push_back(user);

        }

    }

    recuest * err;

    err = new recuest(RT_FIND_USER, rec->sender_id, rec->reciever_id, vector<albom>(),
                               vector<QPixmap>(), users, LoginData(),
                               "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::getPictures( recuest * rec, QTcpSocket * clientSocket){

    QString id = QString::number(rec->reciever_id);

    QSqlQuery query;

    query.prepare("SELECT pic_id, pic FROM user_17_pic WHERE albom_id = ?");

    query.addBindValue(id);

    qDebug() << id;

    bool b = query.exec();

    if (!b) {

        qDebug() << "не вдається створити таблицю";

    }

    std::vector<QPixmap> temp;

    while(query.next()){

        QPixmap outPixmap = QPixmap();

        outPixmap.loadFromData(query.value(1).toByteArray());

        temp.push_back(outPixmap);
    }

    recuest * err;

    err = new  recuest(RT_GET_PHOTOS_FROM_ALBOM, rec->sender_id, rec->reciever_id, vector<albom>(),
                               temp, vector<RData>(), LoginData(),
                               "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::updatePass(recuest * rec, QTcpSocket * clientSocket){

        QSqlQuery query;

        QString id = QString::number(rec->sender_id);

        query.prepare("SELECT password_hash FROM users WHERE id = ?");

        query.addBindValue(id);

        qDebug() << id;

        bool b = query.exec();

        if (!b) {

            qDebug() << "не вдається створити таблицю";

        }
        QString prep = "";

        while(query.next()){

            prep = query.value(0).toString();

        }

        QString pass = rec->message;

        recuest * err;

        if(pass.compare(prep)==0){

            query.clear();

            query.prepare("UPDATE users SET password_hash = ? WHERE id = ?");

            QString pass2 = QString(rec->logData.login);

            query.addBindValue(pass2);

            query.addBindValue(id);

            bool b = query.exec();

            if (!b) {

                qDebug() << "не вдається створити таблицю";

            }

                err = new  recuest(RT_UPDATE_PASS, rec->sender_id,-1,vector<albom>(),
                                   vector<QPixmap>(),vector<RData>(),LoginData(),
                                  "Успішно змінено"  ,QPixmap());

        }else {

            err = new  recuest(RT_NONE, -1,-1,vector<albom>(),
                               vector<QPixmap>(),vector<RData>(),LoginData(),
                               "Невірний попередній пароль" ,QPixmap());
        }

        QString requestStr = err->serializeRecuest();

        requestStr = requestStr + "@#%$#@";

        cout << "Sending: " << endl << requestStr.toStdString() << endl;

        clientSocket->write(requestStr.toUtf8());

        clientSocket->flush();
}

void MainWindow::updatePhoto(recuest * rec , QTcpSocket * clientSocket){

    RData temp = rec->data.at(0);

    QSqlQuery query;

    QString id = QString::number(rec->sender_id);

    query.prepare("UPDATE users_data SET pic = ?, mainpic = ? WHERE id = ?");

    query.addBindValue(temp.pic);

    query.addBindValue(temp.MainPic);

    query.addBindValue(id);

    bool b = query.exec();

    if (!b) {
        qDebug() << "не вдається створити таблицю";
    }

    QPixmap outPixmap = QPixmap();

    outPixmap.loadFromData(temp.MainPic);

    recuest * err;

    err = new  recuest(RT_UPDATE_PHOTO, rec->sender_id, -1, vector<albom>(),
                               vector<QPixmap>(),vector<RData>(), LoginData(),
                               "" , outPixmap );

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::getAlboms( recuest * rec, QTcpSocket * clientSocket){

    QSqlQuery query;

    QString id = QString::number(rec->sender_id);

    query.prepare("SELECT albom_id,albom_name,albom_pic FROM alboms WHERE user_id = ?");

    query.addBindValue(id);

    bool b = query.exec();

    if (!b) {
        qDebug() << "не вдається створити таблицю";
    }

    vector<albom> alboms;

    while(query.next()){

        albom x;

        x.albom_id = query.value(0).toInt();

        x.albom_name = query.value(1).toString();

        x.albom_pic = query.value(2).toByteArray();

        alboms.push_back(x);
    }

    recuest * err;

    err = new  recuest(RT_GET_ALBOMS, rec->sender_id, -1, alboms,
                               vector<QPixmap>(),vector<RData>(), LoginData(),
                               "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::updateInformation(recuest * rec, QTcpSocket * clientSocket){

    QSqlQuery query;

    RData temp = rec->data.at(0);

    QString id = QString::number(rec->sender_id);

    query.prepare("UPDATE users_data SET name = ?, surname = ?, city = ?, religion = ?, date = ?, hobby = ? WHERE id = ?");

    query.addBindValue(temp.name);

    query.addBindValue(temp.surname);

    query.addBindValue(temp.city);

    query.addBindValue(temp.religion);

    query.addBindValue(temp.birth);

    query.addBindValue(temp.hobby);

    query.addBindValue(id);

    bool b = query.exec();

    if (!b) {
        qDebug() << "не вдається створити таблицю";
    }

    recuest * err;

    err = new  recuest(RT_UPDATE_USER, rec->sender_id, -1 , vector<albom>(),
                                   vector<QPixmap>(),rec->data, LoginData(),
                                   "Successfully updated" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::getInformation( recuest * rec, QTcpSocket * clientSocket){

    QSqlQuery query;

    QString id = QString::number(rec->sender_id);

    query.prepare("SELECT name,surname,mainpic,city,religion,date,hobby FROM users_data WHERE id = ?");

    query.addBindValue(id);

    qDebug() << id;

    bool b = query.exec();

    if (!b) {

        qDebug() << "не вдається створити таблицю";

    }

    vector<RData> vec;

    RData temp;

    while(query.next()){

        temp.name = query.value(0).toString();

        temp.surname = query.value(1).toString();

        temp.MainPic = query.value(2).toByteArray();

        temp.city = query.value(3).toString();

        temp.religion = query.value(4).toString();

        temp.birth = query.value(5).toString();

        temp.hobby = query.value(6).toString();

    }

    vec.push_back(temp);

    recuest * err;

    err = new  recuest(RT_GET_INFORMATION, rec->sender_id, -1 , vector<albom>(),
                                   vector<QPixmap>(),vec, LoginData(),
                                   "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::getFullInfo(recuest * rec , QTcpSocket * clientSocket){

    QSqlQuery query;

    QString id = QString::number(rec->sender_id);

    query.prepare("SELECT name,surname,city,religion,date,hobby FROM users_data WHERE id = ?");

    query.addBindValue(id);

    bool b = query.exec();

    if (!b) {

        qDebug() << "не вдається створити таблицю";

    }
    vector <RData> users;
    while(query.next()){

        RData user;
        user.id = rec->sender_id;
        user.name = query.value(0).toString();
        user.surname =  query.value(1).toString();
        user.city = query.value(2).toString();
        user.religion = query.value(3).toString();
        user.birth = query.value(4).toString();
        user.hobby = query.value(5).toString();
        users.push_back(user);

    }

    recuest * err;

    err = new  recuest(RT_FULL_INFO, rec->sender_id, -1 , vector<albom>(),
                                   vector<QPixmap>(),users, LoginData(),
                                   "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::getAlbomsAndInfo(recuest * rec , QTcpSocket * clientSocket){

    QSqlQuery query;

    QString id = QString::number(rec->sender_id);

    query.prepare("SELECT name,surname,city,religion,date,hobby,mainpic FROM users_data WHERE id = ?");

    query.addBindValue(id);

    bool b = query.exec();

    if (!b) {

        qDebug() << "не вдається створити таблицю";

    }
    vector <RData> users;
    while(query.next()){

        RData user;
        user.name = query.value(0).toString();
        user.surname =  query.value(1).toString();
        user.city = query.value(2).toString();
        user.religion = query.value(3).toString();
        user.birth = query.value(4).toString();
        user.hobby = query.value(5).toString();
        user.MainPic = query.value(6).toByteArray();
        users.push_back(user);

    }

    query.clear();

    query.prepare("SELECT albom_id,albom_name,albom_pic FROM alboms WHERE user_id = ?");

    query.addBindValue(id);

    b = query.exec();

    if (!b) {

        qDebug() << "не вдається створити таблицю";

    }

    vector<albom> alboms;

    while(query.next()){

        albom x;

        x.albom_id = query.value(0).toInt();

        x.albom_name = query.value(1).toString();

        x.albom_pic = query.value(2).toByteArray();

        alboms.push_back(x);

    }

    recuest * err;

    err = new  recuest(RT_GET_ALBOMS_AND_DATA, rec->sender_id, -1 , alboms,
                                   vector<QPixmap>(),users, LoginData(),
                                   "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}


void MainWindow::removeAlboms(recuest * rec, QTcpSocket * clientSocket){

    QSqlQuery query;

    for(int j = 0; j < rec->alboms.size(); j++){

        albom temp = rec->alboms.at(j);

        query.clear();

        query.prepare("DELETE FROM user_17_pic WHERE albom_id = ?");

        query.addBindValue(temp.albom_id);

        bool b = query.exec();

        if (!b) {

            qDebug() << "не вдається видалити";

        }

        query.clear();

        query.prepare("DELETE FROM alboms WHERE albom_id = ?");

        query.addBindValue(temp.albom_id);

        b = query.exec();

        if (!b) {
            qDebug() << "не вдається видалити";
        }

    }

    recuest * err;

    err = new  recuest(RT_REMOVE_ALBOM, rec->sender_id, -1 , vector<albom>(),
                       vector<QPixmap>(),vector<RData>(), LoginData(),
                       "" ,QPixmap());

    QString requestStr = err->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}


void MainWindow::getUsers(recuest * rec, QTcpSocket * clientSocket){
    QSqlQuery query;

    query.prepare("SELECT login FROM users");

    bool b = query.exec();
    if (!b) {
        qDebug() << "не вдається створити таблицю";
    }
    vector<RData> temp;

    while(query.next()){
        RData user;
        user.name = query.value(0).toString();
        user.birth = "";
        user.city = "";
        user.hobby = "";
        user.religion = "";
        user.id = -1;
        user.surname = "";
        user.MainPic = "";
        user.pic = "";
        temp.push_back(user);
    }

    LoginData tmp;
    tmp.login = "";
    tmp.password = "";
    recuest * err;
        err = new  recuest(RT_GET_USERS, -1, -1 , vector<albom>(),
                                   vector<QPixmap>(),temp, tmp,
                                   "" ,QPixmap());

        QString requestStr = err->serializeRecuest();
        requestStr = requestStr + "@#%$#@";
            cout << "Sending: " << endl << requestStr.toStdString() << endl;
            clientSocket->write(requestStr.toUtf8());
            clientSocket->flush();

}


void MainWindow::checkUser(recuest * rec, QTcpSocket * clientSocket){

    User * user = storage->loginUser(rec->logData);
    LoginData tmp;
    tmp.login = rec->logData.login;
    tmp.password = "";
    recuest * err;
    if (user == nullptr) {
            err = new  recuest(RT_NONE, -1,-1,vector<albom>(),
                                       vector<QPixmap>(),vector<RData>(),tmp,
                                       "Невірне ім'я користувача, або пароль" ,QPixmap());
     } else {
        err = new  recuest(RT_CHEAK_LOGIN, user->id,-1,vector<albom>(),
                                   vector<QPixmap>(),vector<RData>(),tmp,
                                   "" ,QPixmap());
    }

        QString requestStr = err->serializeRecuest();
        requestStr = requestStr + "@#%$#@";
            cout << "Sending: " << endl << requestStr.toStdString() << endl;
            clientSocket->write(requestStr.toUtf8());
            clientSocket->flush();
}

void MainWindow::addNewUser(recuest * rec, QTcpSocket * clientSocket){


    LoginData tmp;
    tmp.login = rec->logData.login;
    tmp.password = "";

    recuest * mess;

    try {
        QString n1 = rec->data.at(1).name;

        QString n2 = rec->data.at(1).surname;

        User * user = storage->registerUser(rec->data.at(0),rec->logData,n1,n2);

        QString id = QString::number(user->id);

        QString tableName = "user_"+id;

        QSqlQuery query;

        query.prepare("SELECT id FROM users_data");

        QString recues = "";

        if (!query.exec()) {

            throw query.lastError();

        }

        while(query.next()){

            if(query.value(0).toInt()!=user->id){

                recues = recues +" id" + QString::number(query.value(0).toInt()) + " TEXT" + "," +" ";

                QSqlQuery temp;

                QString space = " ";

                QString updateTables = "ALTER TABLE" + space + "user_" + QString::number(query.value(0).toInt()) + " ADD " + "id"+id+ " TEXT";

                temp.prepare(updateTables);

                temp.exec();
            }
        }

        if(recues.length()>2){

            recues.chop(2);

        }
        query.clear();

        QString full_rec = "CREATE TABLE " + tableName + "(" + recues + ")";

        query.prepare(full_rec);

        if (!query.exec()) {

            throw query.lastError();

        }

        mess = new recuest(RT_ADD_USER, user->id,-1,vector<albom>(),
                            vector<QPixmap>(),vector<RData>(), tmp,
                            "" ,QPixmap());

    } catch (const QSqlError & err) {

        mess = new  recuest(RT_NONE, -1,-1,vector<albom>(),
                            vector<QPixmap>(),vector<RData>(),tmp,
                            err.text() ,QPixmap());
    }


    QString requestStr = mess->serializeRecuest();

    requestStr = requestStr + "@#%$#@";

    cout << "Sending: " << endl << requestStr.toStdString() << endl;

    clientSocket->write(requestStr.toUtf8());

    clientSocket->flush();

}

void MainWindow::onBytesWritten(qint64 n) {
    cout << "bytes written" << endl;
}

void MainWindow::onClientDisconnected() {
    cout << "Client disconnected " << endl;
}
