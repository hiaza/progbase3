#include "storagemanager.h"

#include <QCryptographicHash>
#include <QVariant>
#include <QFileDialog>
#include <QBuffer>
#include <QDebug>
storageManager::storageManager(QString path, QObject *parent) : QObject(parent)
{
   db = QSqlDatabase::addDatabase("QSQLITE");
   db.setDatabaseName(path);
}

User * storageManager::registerUser(RData & data, LoginData & priv, QString Quest, QString Answer) {
    QSqlQuery query;
    query.prepare("INSERT INTO users (login, password_hash, question, answer) VALUES (:login, :hash, :question, :answer)");
    query.bindValue(":login", priv.login);
    // @todo hash pass
    QByteArray hashArray = QCryptographicHash::hash(priv.password.toUtf8(), QCryptographicHash::Md5);
    QString passwordHash = QString(hashArray.toHex());
    query.bindValue(":hash", passwordHash);
    query.bindValue(":question", Quest);

    QByteArray hashArray2 = QCryptographicHash::hash(Answer.toUtf8(), QCryptographicHash::Md5);
    QString answerHash = QString(hashArray2.toHex());
    query.bindValue(":answer", answerHash);

    if (!query.exec()) {
        throw query.lastError();
    }

    User * user = new User();
    user->id = query.lastInsertId().value<int>();
    user->login = priv.login;
    user->role = UserRole::SimpleUser;

    query.prepare("INSERT INTO users_data (id, name, surname, pic, mainpic, city, religion, date, hobby) "
                  "VALUES (:id, :name, :surname, :pic, :mainpic, :city, :religion, :date, :hobby)");
    query.bindValue(":id", user->id);
    query.bindValue(":name", data.name);
    query.bindValue(":surname", data.surname);
    query.bindValue(":pic", data.pic);
    query.bindValue(":mainpic", data.MainPic);
    query.bindValue(":city", data.city);
    query.bindValue(":religion", data.religion);
    query.bindValue(":date", data.birth);
    query.bindValue(":hobby", data.hobby);
    if (!query.exec()) {
        throw query.lastError();
    }


    return user;
}

User * storageManager::loginUser(LoginData & data) {
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE login = :login AND password_hash = :hash");
    query.bindValue(":login", data.login);
    QByteArray hashArray = QCryptographicHash::hash(data.password.toUtf8(), QCryptographicHash::Md5);
    QString passwordHash = QString(hashArray.toHex());
    query.bindValue(":hash", passwordHash);
    if (!query.exec()) {
        throw query.lastError();
    }
    if (query.next()) {
       User * user = new User();
       user->id = query.value("id").toInt();
       user->login = query.value("login").toString();
       user->role = (UserRole)query.value("role").toInt();
       return user;
    }
    return nullptr;
}

//bool storageManager::addToPicBase(int id,QByteArray pic) {

//    QSqlQuery query;
//    query.prepare("INSERT INTO user_:id_pic (pic) VALUES (:pic)");
//    query.bindValue(":id", QString::number(id));
//    query.bindValue(":pic", pic);
//    if (!query.exec()) {
//        throw query.lastError();
//        return false;
//    }
//    return true;
//}

bool storageManager::open() {
    return db.open();
}

void storageManager::close() {
    db.close();
}
